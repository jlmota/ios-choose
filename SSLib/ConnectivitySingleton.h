//
//  ConnectivitySingleton.h
//  PoweradeIphone
//
//  Created by Simão Seiça on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kINTERNET_CONNECTION @"internet_connection"

@interface ConnectivitySingleton : NSObject

+ (ConnectivitySingleton *) sharedInstance;

- (BOOL) isOnline;

@end
