//
//  ServerManager.m
//  Cocacola
//
//  Created by Simão Seiça {ext} on 8/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ServerManager.h"
#import "AFHTTPRequestOperationManager.h"


@implementation ServerManager

//
// Make synchronous request
//
+ (NSString *) connectServerWithUrl:(NSURL *)url{
    
    NSURLResponse *response;
	NSError *error;
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:60];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	NSData *urlData = [NSURLConnection sendSynchronousRequest:urlRequest
                                            returningResponse:&response
                                                        error:&error];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
	return [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
}

+ (NSData *) getDataFromUrl:(NSURL *)url{
    
    NSURLResponse *response;
	NSError *error;
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                            timeoutInterval:60];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	NSData *urlData = [NSURLConnection sendSynchronousRequest:urlRequest
                                            returningResponse:&response
                                                        error:&error];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
	return urlData;
}

+ (UIImage *) loadImageFromURL:(NSString *) url{
    
    DLog(@"[INFO]Loading image: %@", url);
    
    if(url){
        if(![UIApplication sharedApplication].networkActivityIndicatorVisible){
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        }
        
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfURL:
                        [NSURL URLWithString: [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                                             options:0 error:&error];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if(error){
            DLog(@"[ERROR]Error downloading image: %@. Url:%@",[error localizedDescription], url);
        }else{
            
            return [UIImage imageWithData: data];
        }
    }
    return nil;
}

@end
