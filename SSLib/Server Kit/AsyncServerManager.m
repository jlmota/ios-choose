//
//  AsyncServerManager.m
//  LisbonWeek
//
//  Created by Simão Seiça on 9/25/12.
//  Copyright (c) 2012 Fullsix. All rights reserved.
//

#import "AsyncServerManager.h"

@interface AsyncServerManager ()


@property (nonatomic, strong) NSMutableData * receivedData;
@property (nonatomic, retain) NSURLConnection *connection;

@end

@implementation AsyncServerManager

@synthesize isWorking;
@synthesize notificationKey;
@synthesize connection;

- (void) connectServerWithUrl:(NSURL *)url andContent:(NSString *) content{

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    NSData *requestData = [NSData dataWithBytes:[content UTF8String]
                                         length:[content length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody: requestData];
    
   self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (self.connection) {
        
        self.isWorking = YES;
        _receivedData = [NSMutableData data];
    }
}

#pragma mark URLRequest delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    [self.receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [self.receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    
    // inform the user
    DLog(@"[ERROR]Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
   
    self.isWorking = NO;
}

- (void)connectionDidFinishLoading:(NSURLConnection *) _connection
{
    NSString *message = [[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];
    DLog(@"[INFO][RESPONSE] %@", message);
        
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:self.notificationKey object:message];
    });
    
    self.isWorking = NO;
}
@end
