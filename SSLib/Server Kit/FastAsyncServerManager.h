//
//  AsyncServerManager.h
//  LisbonWeek
//
//  Created by Simão Seiça on 9/25/12.
//  Copyright (c) 2012 Fullsix. All rights reserved.
//

#import "ServerManager.h"

@interface FastAsyncServerManager : ServerManager


- (void) connectServerWithUrl:(NSURL *) url andContent:(NSString *) content toKey:(NSString *) notificationKey;

@end
