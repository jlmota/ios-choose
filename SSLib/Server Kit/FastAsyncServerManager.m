//
//  AsyncServerManager.m
//  LisbonWeek
//
//  Created by Simão Seiça on 9/25/12.
//  Copyright (c) 2012 Fullsix. All rights reserved.
//

#import "FastAsyncServerManager.h"

@interface FastAsyncServerManager ()

@end

@implementation FastAsyncServerManager

- (void) connectServerWithUrl:(NSURL *) url andContent:(NSString *) content toKey:(NSString *) notificationKey{
    
    NSData *requestData = [NSData dataWithBytes:[content UTF8String]
                                         length:[content length]];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody: requestData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        NSInteger responseCode = [(NSHTTPURLResponse *)response statusCode];
        
        if (!error && responseCode == 200) {
            
            NSString *message = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            DLog(@"[INFO][RESPONSE] %@", message);
            
            if (message) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:notificationKey object:message];
                });
            } else {
                
                DLog(@"[ERROR] Error updating the data: %@", message);
                [[NSNotificationCenter defaultCenter] postNotificationName:notificationKey object:nil];
            }
        } else {
            
            DLog(@"[ERROR] Error updating the data. Response code:%d", responseCode);
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationKey object:nil];
        }
    }];
}

@end
