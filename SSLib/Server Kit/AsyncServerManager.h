//
//  AsyncServerManager.h
//  LisbonWeek
//
//  Created by Simão Seiça on 9/25/12.
//  Copyright (c) 2012 Fullsix. All rights reserved.
//

#import "ServerManager.h"

@interface AsyncServerManager : ServerManager


@property (nonatomic, assign) BOOL isWorking;
@property (nonatomic, retain) NSString * notificationKey;

- (void) connectServerWithUrl:(NSURL *)url andContent:(NSString *) content;

@end
