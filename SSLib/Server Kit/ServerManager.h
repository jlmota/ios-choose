//
//  ServerManager.h
//  Cocacola
//
//  Created by Simão Seiça {ext} on 8/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ServerManager : NSObject {
    
}

+ (NSString *) connectServerWithUrl:(NSURL *)url;

+ (NSData *) getDataFromUrl:(NSURL *)url;

+ (UIImage *) loadImageFromURL:(NSString *) url;

@end
