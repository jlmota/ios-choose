//
//  DataStorageManager.h
//  MIsReservas
//
//  Created by Simão Seiça on 28/01/11.
//  Copyright 2011 LogiTravel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataStorageManager.h"


@interface DataStorageManager : NSObject {

}

/**
 *	Save data on UserDefaults.
 *
 **/
+ (BOOL)saveObject:(id)object withKey:(NSString *)key;

/**
 *	Get object data from UserDefaults.
 *
 **/
+ (id)getObjectForKey:(NSString *)key;

@end
