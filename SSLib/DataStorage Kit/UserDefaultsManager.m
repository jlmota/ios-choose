//
//  UserDefaultsManager.m
//  MIsReservas
//
//  Created by Simão Seiça on 28/01/11.
//  Copyright 2011 LogiTravel. All rights reserved.
//

#import "UserDefaultsManager.h"


@implementation UserDefaultsManager


/**
 *	Save data on UserDefaults.
 *
 **/
+ (BOOL)saveObject:(id)object withKey:(NSString *)key{
	
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
	if (standardUserDefaults) {
		NSData *data = [NSKeyedArchiver archivedDataWithRootObject:object];
		[standardUserDefaults setObject:data forKey:key];
		[standardUserDefaults synchronize];
		
		return YES;
	}else {
		return NO;
	}
}

/**
 *	Get object data from UserDefaults.
 *
 **/
+ (id)getObjectForKey:(NSString *)key{
	
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	id val = nil;
	
	if (standardUserDefaults) {
		NSData *data = [standardUserDefaults objectForKey:key];
		if (data!=0) {
			val = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		}
	}
	
	return val;
}

@end
