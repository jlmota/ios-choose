//
//  EventManager.h
//  LisbonWeek
//
//  Created by Simão Seiça on 9/27/12.
//  Copyright (c) 2012 Fullsix. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kCALENDAR_NOTIFICATION @"calendar_notification"

#define kEVENT_DATA_KEY @"data_key"
#define kEVENT_STATUS_KEY @"status_key"

@interface EventManager : NSObject

+ (void) saveEventWithTitle:(NSString *) title startDate:(NSDate *) startDate andEndDate:(NSDate *) endDate
                   location:(NSString *) location andKey:(id) key;

@end
