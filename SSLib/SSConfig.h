

#import <Foundation/Foundation.h>

#define DEBUG_METHODS 1

#ifdef DEBUG
#define DLog(...) NSLog(@"[%s] %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#define DDLog(...) NSLog("%@ %@",[self class], NSStringFromSelector(_cmd),__VA_ARGS__)
#define DDDLog(...) NSLog(@"%@: %@", NSStringFromSelector(_cmd), self);
#define SLog(format, ...) CFShow([NSString stringWithFormat:format, ## __VA_ARGS__]);
#define SAlert(format, ...)  {UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n line: %d ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:format, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Done" otherButtonTitles:nil]; [alert show];}
#else
#define DLog(...)
#define DDLog(...)
#define DDDLog(...)
#define SLog(format, ...)
#define SAlert(format, ...)
#endif


#ifdef DEBUG_METHODS
#define SSFUNC DLog(@"%s", __PRETTY_FUNCTION__);
#else
#define SSFUNC
#endif