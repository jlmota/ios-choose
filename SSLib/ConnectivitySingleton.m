//
//  ConnectivitySingleton.m
//  PoweradeIphone
//
//  Created by Simão Seiça on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ConnectivitySingleton.h"
#import "Reachability.h"

@interface ConnectivitySingleton ()

@property (nonatomic, strong) Reachability* internetReachable;
@property (nonatomic, strong) Reachability* hostReachable;

@end

@implementation ConnectivitySingleton

+ (ConnectivitySingleton *)sharedInstance {
    static ConnectivitySingleton *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[ConnectivitySingleton alloc] init];
    });
    
    return _instance;
}

- (id) init {
    self = [super init];
    if (self) {
        
        [self initReachabilityHelpers];
    }
    return self;
}

#pragma mark - Internet Connection Methods

- (void) initReachabilityHelpers{
    
    // check for internet connection
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkNetworkStatus:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    //use this next time
    //Reachability* wifiReach = [Reachability reachabilityForLocalWiFi];
    _internetReachable = [Reachability reachabilityForInternetConnection];
    
    [self.internetReachable startNotifier];
    
    // check if a pathway to a random host exists
    _hostReachable = [Reachability reachabilityWithHostname: @"www.google.pt"];
    [self.hostReachable startNotifier];
}

- (BOOL) isOnline{
    
    if([self.internetReachable currentReachabilityStatus] != NotReachable &&
       [_hostReachable currentReachabilityStatus] != NotReachable){
        
        return YES;
    }
    
    DLog(@"[WARNING] No Internet Connection.");
    return NO;
}

- (void) checkNetworkStatus:(NSNotification *)notice
{
    
    // called after network status changes
    
    NetworkStatus internetStatus = [self.internetReachable currentReachabilityStatus];
    switch (internetStatus)
    
    {
        case NotReachable:
        {
            DLog(@"[WARNING]The internet is down.");
            break;
        }
        case ReachableViaWiFi:
        {
            DLog(@"[INFO]The internet is working via WIFI.");
            break;
            
        }
        case ReachableViaWWAN:
        {
            DLog(@"[INFO]The internet is working via WWAN.");
            break;
            
        }
    }
    
    NetworkStatus hostStatus = [_hostReachable currentReachabilityStatus];
    switch (hostStatus)
    
    {
        case NotReachable:
        {
            DLog(@"[WARNING]A gateway to the host server is down.");
            break;
            
        }
        case ReachableViaWiFi:
        {
            DLog(@"[INFO]A gateway to the host server is working via WIFI.");
            break;
            
        }
        case ReachableViaWWAN:
        {
            DLog(@"[INFO]A gateway to the host server is working via WWAN.");
            break;
            
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kINTERNET_CONNECTION
                                                            object:[NSNumber numberWithInt:ReachableViaWWAN]];
    });
}

@end
