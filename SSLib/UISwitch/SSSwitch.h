#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface SSSwitch : UIControl

@property (strong, nonatomic) UIImage *backgroundImage UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIImage *knobImage UI_APPEARANCE_SELECTOR;
@property (strong, nonatomic) UIImage *highlightedKnobImage UI_APPEARANCE_SELECTOR;
@property (assign, readwrite, nonatomic) CGSize knobOffset UI_APPEARANCE_SELECTOR;
@property (nonatomic, getter=isOn) BOOL on;
@property (strong, nonatomic) NSString *offImage;
@property (strong, nonatomic) NSString *onImage;
@property (strong, nonatomic) NSString *offBgImage;
@property (strong, nonatomic) NSString *onBgImage;

- (void)setOn:(BOOL)on;
- (void)setOn:(BOOL)on animated:(BOOL)animated;

@end
