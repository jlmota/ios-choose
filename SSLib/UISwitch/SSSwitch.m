#import "SSSwitch.h"

@implementation SSSwitch{
    
    CGFloat _initialOffset;
    UIView *_containerView;
    UIView *backgroundView;
    UIImageView *_knobView;
    UIImageView *onBgImageView;
    UIImageView *offBgImageView;
    BOOL _isOn;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.on = NO;
        _knobOffset = CGSizeMake(0, 0);
        
        _containerView = [[UIView alloc] initWithFrame:self.bounds];
        _containerView.clipsToBounds = YES;
        [self addSubview:_containerView];
        
        _knobView = [[UIImageView alloc] initWithFrame:CGRectNull];
        [self addSubview:_knobView];
        
        [self initBackground:frame];
    }
    return self;
}

- (void) initBackground:(CGRect) frame{
    
    backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width * 2, frame.size.height)];
    backgroundView.userInteractionEnabled = YES;
    [_containerView addSubview:backgroundView];
    
    onBgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [backgroundView addSubview:onBgImageView];
    
    offBgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [backgroundView addSubview:offBgImageView];
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizerDidChange:)];
    [backgroundView addGestureRecognizer:panGestureRecognizer];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerDidChange:)];
    [backgroundView addGestureRecognizer:tapGestureRecognizer];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = backgroundView.frame;
    frame.origin.x = _isOn ? 0 : self.offPosition;
    backgroundView.frame = frame;
    
    // _backgroundImageView.image = _backgroundImage;
    _knobView.image = _knobImage;
    
    UIImage *bgOnImage = [UIImage imageNamed:_onBgImage];
    UIImage *bgOffImage = [UIImage imageNamed:_offBgImage];
    
    onBgImageView.image = bgOnImage;
    onBgImageView.frame = CGRectMake(0, 0, bgOnImage.size.width, bgOnImage.size.height);
    offBgImageView.image = bgOffImage;
    offBgImageView.frame = CGRectMake(bgOffImage.size.width*2, 0, bgOffImage.size.width, bgOffImage.size.height);
    
    //_backgroundImageView.frame = CGRectMake(0, 0, _backgroundImage.size.width, _backgroundImage.size.height);
    // frame = _backgroundView.frame;
    CGRect knobFrame = CGRectMake(0, 0, _knobImage.size.width, _knobImage.size.height);
    knobFrame.origin.x = frame.origin.x + self.frame.size.width - knobFrame.size.width + _knobOffset.width;
    knobFrame.origin.y = _knobOffset.height;
    _knobView.frame = knobFrame;
    
}

- (CGFloat)offPosition
{
    return -self.frame.size.width + _knobImage.size.width - _knobOffset.width*2;
}

- (BOOL)isOn
{
    return backgroundView.frame.origin.x == 0;
}

- (void)setOn:(BOOL)on animated:(BOOL)animated{
    if (animated) {
        _isOn = on;
        
        [UIView animateWithDuration:0.25 animations:^{
            CGRect frame = backgroundView.frame;
            frame.origin.x = on ? 0 : self.offPosition;
            backgroundView.frame = frame;
            
            CGRect knobFrame = _knobView.frame;
            knobFrame.origin.x = frame.origin.x + self.frame.size.width - knobFrame.size.width + _knobOffset.width;
            _knobView.frame = knobFrame;
        }];
    } else {
        
        [self setOn:on];
    }
}

- (void)setOn:(BOOL)on{
    _isOn = on;
    [self setKnobImage:[UIImage imageNamed: _isOn ? _onImage : _offImage]];
    [self setNeedsLayout];
}

#pragma mark - Apperance

- (void)setKnobOffset:(CGSize)knobOffset{
    
    _knobOffset = knobOffset;
    [self setNeedsLayout];
}

#pragma mark - Gesture recognizers

- (void)panGestureRecognizerDidChange:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:backgroundView];
	if (recognizer.state == UIGestureRecognizerStateBegan) {
		_initialOffset = backgroundView.frame.origin.x;
	}
	
	float x = translation.x;
	x = translation.x + _initialOffset;
    
    CGRect frame = backgroundView.frame;
    frame.origin.x = x;
    if (frame.origin.x > 0) {
        frame.origin.x = 0;
    }
    if (frame.origin.x < self.offPosition)
        frame.origin.x = self.offPosition;
    backgroundView.frame = frame;
    
    CGRect knobFrame = _knobView.frame;
    knobFrame.origin.x = frame.origin.x + self.frame.size.width - knobFrame.size.width + _knobOffset.width;
    _knobView.frame = knobFrame;
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        if (_highlightedKnobImage)
            _knobView.image = _highlightedKnobImage;
    }
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        _knobView.image = [UIImage imageNamed:_isOn ?  _offImage : _onImage];
        
        if (frame.origin.x > -self.frame.size.width / 2) {
            frame.origin.x = 0;
            _isOn = YES;
            _knobView.image = [UIImage imageNamed:_isOn ? _onImage :  _offImage];
        } else {
            frame.origin.x = self.offPosition;
            _isOn = NO;
            _knobView.image = [UIImage imageNamed:_isOn ?  _onImage :  _offImage];
        }
        
        [UIView animateWithDuration:0.1 animations:^{
            backgroundView.frame = frame;
            
            CGRect knobFrame = _knobView.frame;
            knobFrame.origin.x = frame.origin.x + self.frame.size.width - knobFrame.size.width + _knobOffset.width;
            _knobView.frame = knobFrame;
        }];
        
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

- (void)tapGestureRecognizerDidChange:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded) {

        _isOn = !_isOn;
        _knobView.image = [UIImage imageNamed:_isOn ? _onImage : _offImage];
        
        CGRect frame = backgroundView.frame;
        
        if (frame.origin.x == 0) {
            frame.origin.x = self.offPosition;
        } else {
            frame.origin.x = 0;
        }
        [UIView animateWithDuration:0.15 animations:^{
            backgroundView.frame = frame;
            
            CGRect knobFrame = _knobView.frame;
            knobFrame.origin.x = frame.origin.x + self.frame.size.width - knobFrame.size.width + _knobOffset.width;
            _knobView.frame = knobFrame;
        }];
        
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{

}

@end
