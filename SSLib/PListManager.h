//
//  PListManager.h
//  PoweradeIphone
//
//  Created by Simão Seiça on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PListManager : NSObject

+ (NSDictionary *) getDicPListWithName:(NSString *) name;

+ (NSArray *) getArrayPListWithName:(NSString *) name;

@end
