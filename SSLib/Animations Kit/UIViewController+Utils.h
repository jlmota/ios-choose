//
//  ViewController+Utils.h
//  McDonalds
//
//  Created by Simão Seiça on 1/17/13.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utils)

- (void) pushViewController:(UIViewController *) viewController;

- (void) popViewController;


@end
