//
//  UIDevice-Utils.m
//  SSLib
//
//  Created by Simão Seiça on 12/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UIDevice+Utils.h"

@implementation UIDevice (Utils)


+ (BOOL) isiOSVersionHigherThan:(NSString *)requiredVersion
{
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    
    if ([currSysVer compare:requiredVersion options:NSNumericSearch] != NSOrderedAscending)
    {
        return YES;
    }
    
    return NO;
}

+ (BOOL) isRetina{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]
        && [[UIScreen mainScreen] scale] == 2.0) {
        
        return YES;
    }
    return NO;
}

+ (BOOL) isIPad{
    
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);   
}

+ (BOOL) isIPhone{
    
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

+ (BOOL) isIPhone5{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    
    return result.height == 568;
}

+ (void) disableBlockScreen{
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

+ (float) getDeviceBatteryLevel{
    
    return [[UIDevice currentDevice] batteryLevel];
}

+ (NSString *) getDeviceSystemVersion{
    
    return [[UIDevice currentDevice] systemVersion];
}

+ (NSString *) getDeviceModel{
    
    return [[UIDevice currentDevice] model];
}


@end
