//
//  UIDevice-Utils.h
//  SSLib
//
//  Created by Simão Seiça on 12/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIDevice (Utils)

+ (BOOL) isiOSVersionHigherThan:(NSString *)requiredVersion;

+ (BOOL) isIPad;

+ (BOOL) isIPhone;

+ (BOOL) isIPhone5;

+ (void) disableBlockScreen;

+ (BOOL) isRetina;

+ (float) getDeviceBatteryLevel;

+ (NSString *) getDeviceSystemVersion;

+ (NSString *) getDeviceModel;

@end
