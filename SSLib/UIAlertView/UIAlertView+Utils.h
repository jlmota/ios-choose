//
//  NUIView.h
//  NUI
//
//  Created by Tom Benner on 11/20/12.
//  Copyright (c) 2012 Tom Benner. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIAlertView (Utils) {

}

+ (void) showAlertScreenWithTitle:(NSString *) title andMessage:(NSString *) message;

@end
