//
//  NUIView.m
//  NUI
//
//  Created by Tom Benner on 11/20/12.
//  Copyright (c) 2012 Tom Benner. All rights reserved.
//

#import "UIAlertView+Utils.h"

@implementation UIAlertView (Utils)


+ (void) showAlertScreenWithTitle:(NSString *) title andMessage:(NSString *) message {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
