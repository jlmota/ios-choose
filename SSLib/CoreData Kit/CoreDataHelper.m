//
//  CoreDataHelper.m
//  Mcdonalds
//
//  Created by Bruno Sousa on 6/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CoreDataHelper.h"

@implementation CoreDataHelper

#pragma mark -
#pragma mark Retrieve objects

// Fetch objects with a predicate and limit
+(NSMutableArray *)searchObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate *)predicate andSortKey:(NSString*)sortKey andSortAscending:(BOOL)sortAscending andLimit:(int)limit andContext:(NSManagedObjectContext *)managedObjectContext
{
	// Create fetch request
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
	
    [request setEntity:entity];
    [request setReturnsObjectsAsFaults:NO];
    
	// If a predicate was specified then use it in the request
	if (predicate != nil)
		[request setPredicate:predicate];
    
	// If a sort key was passed then use it in the request
	if (sortKey != nil) {
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:sortAscending];
		NSArray * sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
		[request setSortDescriptors:sortDescriptors];
	}
    
    [request setFetchLimit:limit];
    
	// Execute the fetch request
	NSError *error = nil;
	NSMutableArray *mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    
	// If the returned array was nil then there was an error
	if (mutableFetchResults == nil)
		DLog(@"Couldn't get objects for entity %@", entityName);
    
	// Return the results
	return mutableFetchResults;
}

// Fetch objects with a predicate
+(NSMutableArray *)searchObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate *)predicate
                               andSortKey:(NSString*)sortKey andSortAscending:(BOOL)sortAscending andContext:(NSManagedObjectContext *)managedObjectContext
{
	// Create fetch request
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
	
    [request setEntity:entity];
    [request setReturnsObjectsAsFaults:NO];
    
	// If a predicate was specified then use it in the request
	if (predicate != nil)
		[request setPredicate:predicate];
    
	// If a sort key was passed then use it in the request
	if (sortKey != nil) {
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:sortAscending];
		NSArray * sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
		[request setSortDescriptors:sortDescriptors];
	}
    
	// Execute the fetch request
	NSError *error = nil;
	NSMutableArray *mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    
	// If the returned array was nil then there was an error
	if (mutableFetchResults == nil){
        
		DLog(@"Couldn't get objects for entity %@", entityName);
    }
    
	// Return the results
	return mutableFetchResults;
}

#pragma mark - Delete Objects

// Delete all objects for a given entity
+(BOOL)deleteAllObjectsForEntity:(NSString*)entityName inContext:(NSManagedObjectContext *)managedObjectContext
{
    
    return [CoreDataHelper deleteAllObjectsForEntity:entityName withPredicate:nil
                                           inContext: managedObjectContext];
}

// Delete all objects for a given entity
+ (BOOL) deleteAllObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate *) predicate
                         inContext:(NSManagedObjectContext *)managedObjectContext
{
	// Create fetch request
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext:managedObjectContext];
	[request setEntity:entity];
    
    predicate ? [request setPredicate:predicate] : nil;
    
	// Ignore property values for maximum performance
	[request setIncludesPropertyValues:NO];
    
	// Execute the count request
	NSError *error = nil;
	NSArray *fetchResults = [managedObjectContext executeFetchRequest:request error:&error];
    
	// Delete the objects returned if the results weren't nil
	if (fetchResults != nil) {
        
        DLog(@"The app is going to delete %d objects of table '%@'.", [fetchResults count], entityName);
        
		for (NSManagedObject *manObj in fetchResults) {
			[managedObjectContext deleteObject:manObj];
		}
        
	} else {
        
		DLog(@"Couldn't delete objects for entity %@", entityName);
		return NO;
	}
    
	return [managedObjectContext save:nil];
}

+ (NSArray *) getManagedObjects:(NSString *) tableName withPredicate:(NSPredicate *) predicate
                     andSortKey:(NSString*) sortKey inContext:(NSManagedObjectContext *) _managedObjectContext{
    
    return [CoreDataHelper searchObjectsForEntity:tableName
                                    withPredicate:predicate
                                       andSortKey:sortKey
                                 andSortAscending:NO
                                       andContext: _managedObjectContext];
}


+ (NSArray *) getManagedObjects:(NSString *) tableName withPredicate:(NSPredicate *) predicate
                     andSortKey:(NSString*) sortKey isAscending:(BOOL) isAscending
                      inContext:(NSManagedObjectContext *) _managedObjectContext{
    
    return [CoreDataHelper searchObjectsForEntity:tableName
                                    withPredicate:predicate
                                       andSortKey:sortKey
                                 andSortAscending:isAscending
                                       andContext:_managedObjectContext];
}

+ (NSArray *) getManagedObjects:(NSString *) tableName withPredicate:(NSPredicate *) predicate
                      inContext:(NSManagedObjectContext *) _managedObjectContext{
    
    return [CoreDataHelper getManagedObjects:tableName withPredicate: predicate
                                  andSortKey:nil inContext: _managedObjectContext];
}

+ (NSManagedObject *) getManagedObject:(NSString *) tableName byId:(NSNumber *) _id
                             inContext:(NSManagedObjectContext *) _managedObjectContext{
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"tableID==%@", _id];
    
    NSMutableArray* mutableFetchResults = [CoreDataHelper searchObjectsForEntity:tableName
                                                                   withPredicate:predicate andSortKey:nil andSortAscending:NO
                                                                      andContext: _managedObjectContext];
    
    if([mutableFetchResults count] == 1){
        
        return [mutableFetchResults objectAtIndex:0];
    }
    
    //should not have more than 1 and equal 0
    return nil;
}

// Fetch objects without a predicate
+(NSMutableArray *)getObjectsForEntity:(NSString*)entityName withSortKey:(NSString*)sortKey andSortAscending:(BOOL)sortAscending andContext:(NSManagedObjectContext *)managedObjectContext
{
	return [self searchObjectsForEntity:entityName withPredicate:nil andSortKey:sortKey andSortAscending:sortAscending andContext:managedObjectContext];
}

+ (BOOL) deleteManagedObject:(NSString *) tableName byId:(NSNumber *) _id
                   inContext:(NSManagedObjectContext *) _managedObjectContext
{
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"tableID == %@", _id];
    
    NSMutableArray* mutableFetchResults = [CoreDataHelper searchObjectsForEntity:tableName
                                                                   withPredicate:predicate
                                                                      andSortKey:nil
                                                                andSortAscending:NO
                                                                      andContext: _managedObjectContext];
    
    // Delete all matches
    for (int i = 0; i < [mutableFetchResults count]; i++) {
        
        [_managedObjectContext deleteObject:[mutableFetchResults objectAtIndex:i]];
    }
    
    //save in the database
    return [_managedObjectContext save:nil];
}

+ (BOOL) deleteManagedObject:(NSManagedObject *) managedObject
                   inContext:(NSManagedObjectContext *) _managedObjectContext
{
    
    [_managedObjectContext deleteObject:managedObject];
    
#if DEBUG
    DLog(@"[INFO]Object deleted successfully.");
#endif
    
    //save in the database
    return [_managedObjectContext save:nil];
}

@end
