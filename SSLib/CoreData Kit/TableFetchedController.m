//
//  CoreDataManager.m
//  MyDictionary
//
//  Created by Simao Seica on 3/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TableFetchedController.h"
#import <UIKit/UIKit.h>

@implementation TableFetchedController


#pragma mark - TableView Helper

- (NSInteger)getNumberOfSections{
    
    return [[self.fetchedResultsController sections] count];   
}

- (NSInteger) getNumberOfRows{

    return [self getNumberOfRowsInSection:0];
}

- (NSInteger) getNumberOfRowsInSection:(NSInteger) section{
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] 
                                                    objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (NSManagedObject *)getManagedObjectAtIndexPath:(NSIndexPath *)indexPath{
    //todo is it really necessary?
    [self fetchedResultsController];
    
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (NSManagedObject *)getManagedObjectAtIndex:(NSInteger) index{
    
    return [self getManagedObjectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
}

- (BOOL)deleteManagedObjectAtIndexPath:(NSIndexPath *)indexPath{
    // Delete the managed object for the given index path
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]){

        DLog(@"[ERROR] Fail deleting the object: %@, %@", error, [error userInfo]);
        return NO;
    }else{
        
        DLog(@"[INFO] Object deleted successfully.");
        return YES;
    }
}

@end
