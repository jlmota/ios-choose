//
//  FetchedController.h
//  Cocacola
//
//  Created by Simão Seiça {ext} on 8/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol FetchedControllerProtocol;

@interface FetchedController : NSObject <NSFetchedResultsControllerDelegate> {
    
}

@property (nonatomic, retain) id<FetchedControllerProtocol> delegate;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSString * tableName;
@property (nonatomic, retain) NSArray * sortColumns;
@property (nonatomic, retain) NSPredicate *predicate;
@property (nonatomic, assign) BOOL isAscending;
@property (nonatomic, assign) NSInteger resultsLimit;


- (id) initWithTable:(NSString *) _tableName;

- (id) initWithTable:(NSString *) _tableName predicate:(NSPredicate *) _predicate 
       andSortColumns:(NSArray *) _sortColumns;

- (id) initWithTable:(NSString *) _tableName predicate:(NSPredicate *) _predicate
      andSortColumns:(NSArray *) _sortColumns isAscending:(BOOL) _isAscending andLimit:(NSInteger) _resultsLimit;

- (NSManagedObject *)getNewManagedObjectWithName:(NSString *) name;

- (void) updateFetchedControllerWithSortColumns:(NSArray *) _sortColumns ascending:(BOOL) ascending;

- (void) updatePredicates:(NSPredicate *) predicate1,...;


@end


@protocol FetchedControllerProtocol <NSObject>

- (void) fetchedControllerDidChangeContent;

@optional
- (void) fetchedControllerWillChangeContent;

- (void) fetchedControllerDidChangeSection:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType) type;

- (void) fetchedControllerDidChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath;



@end