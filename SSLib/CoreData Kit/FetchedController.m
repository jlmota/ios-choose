//
//  FetchedController.m
//  Cocacola
//
//  Created by Simão Seiça {ext} on 8/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FetchedController.h"
#import "ContextSingleton.h"


@implementation FetchedController

@synthesize delegate;
@synthesize fetchedResultsController;
@synthesize managedObjectContext;
@synthesize tableName;
@synthesize sortColumns;
@synthesize predicate;
@synthesize isAscending;
@synthesize resultsLimit;

#pragma mark - Init

- (id) initWithTable:(NSString *) _tableName{
    
    return [self initWithTable:_tableName predicate:nil andSortColumns:nil isAscending:NO andLimit:0];
}

- (id) initWithTable:(NSString *) _tableName predicate:(NSPredicate *) _predicate
      andSortColumns:(NSArray *) _sortColumns{
    
    return [self initWithTable:_tableName predicate:_predicate andSortColumns:_sortColumns isAscending:NO andLimit:0];
}

- (id) initWithTable:(NSString *) _tableName predicate:(NSPredicate *) _predicate
      andSortColumns:(NSArray *) _sortColumns isAscending:(BOOL) _isAscending andLimit:(NSInteger) _resultsLimit{
    self = [super init];
    if (self) {
        
        self.tableName = _tableName;
        self.sortColumns = _sortColumns;
        self.predicate = _predicate;
        self.isAscending = _isAscending;
        self.resultsLimit = _resultsLimit;
        
        self.managedObjectContext = [ContextSingleton sharedInstance].managedObjectContext;
        
        //init Fetched Controller
        NSError *error = nil;
        if (![[self fetchedResultsController] performFetch:&error])
        {
            DLog(@"[ERROR]Unresolved error. %@", [error localizedDescription]);
        }
        
    }
    return self;
}

- (void) updatePredicates:(NSPredicate *) predicate1, ...{
    
    
    id eachObject;
    va_list argumentList;
    if (predicate1) // The first argument isn't part of the varargs list,
    {                                   // so we'll handle it separately.
        
        NSMutableArray * predicateList = [[NSMutableArray alloc] initWithObjects:predicate1, nil];
        va_start(argumentList, predicate1); // Start scanning for arguments after firstObject.
        while ((eachObject = va_arg(argumentList, id))){// As many times as we can get an argument of type "id"
            [predicateList addObject: eachObject]; // that isn't nil, add it to self's contents.
        }
        va_end(argumentList);
        
        self.predicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicateList];
    }
}

#pragma mark - Helpers

- (NSManagedObject *) getNewManagedObjectWithName:(NSString *) name{
    
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    
    return [NSEntityDescription insertNewObjectForEntityForName:[entity name]
                                         inManagedObjectContext:context];
    
}

- (NSFetchedResultsController *) fetchedResultsController{
    if (fetchedResultsController){
        
        return fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:self.tableName
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    //to avoid errors when the predicate is changed
    //[NSFetchedResultsController deleteCacheWithName:@"Root"];
    
    if(predicate != nil){
        
        [fetchRequest setPredicate:self.predicate];
    }
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    self.resultsLimit != 0 ? [fetchRequest setFetchLimit:self.resultsLimit]:nil;
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = nil;
    NSArray *sortDescriptors = nil;
    if(self.sortColumns){
        
        NSMutableArray *sortDescriptors = [[NSMutableArray alloc] init];
        for(NSString * sortColumn in self.sortColumns){
            
            [sortDescriptors addObject:[[NSSortDescriptor alloc] initWithKey:sortColumn ascending:self.isAscending]];
        }
        
        [fetchRequest setSortDescriptors:sortDescriptors];
    }else{
        
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"tableID" ascending:self.isAscending];
        sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
    }
    
    
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    fetchedResultsController = [[NSFetchedResultsController alloc]
                                initWithFetchRequest:fetchRequest
                                managedObjectContext:self.managedObjectContext
                                sectionNameKeyPath:nil cacheName:nil];
    fetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error])
    {
        DLog(@"[ERROR] Error fetching the results. %@",  [error localizedDescription]);
    }
    
    return fetchedResultsController;
}

- (void) updateFetchedControllerWithSortColumns:(NSArray *) _sortColumns ascending:(BOOL) ascending{
    
    self.sortColumns = _sortColumns;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:self.tableName
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    if(self.predicate){
        
        [fetchRequest setPredicate:self.predicate];
    }
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    NSMutableArray *sortDescriptors = nil;
    if(_sortColumns){
        sortDescriptors = [[NSMutableArray alloc] init];
        for(NSString * sortColumn in _sortColumns){
            
            [sortDescriptors addObject:[[NSSortDescriptor alloc] initWithKey:sortColumn ascending:ascending]];
        }
        
        [fetchRequest setSortDescriptors:sortDescriptors];
    }
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]
                                                             initWithFetchRequest:fetchRequest
                                                             managedObjectContext:self.managedObjectContext
                                                             sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error])
    {
        
        DLog(@"[ERROR] Error fetching the results. %@",  [error localizedDescription]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(fetchedControllerWillChangeContent)]){
        
        [self.delegate fetchedControllerWillChangeContent];
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(fetchedControllerDidChangeSection:forChangeType:)]){
        
        [self.delegate fetchedControllerDidChangeSection:(NSUInteger)sectionIndex forChangeType:type];
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    if(self.delegate && [self.delegate
                         respondsToSelector:@selector(fetchedControllerDidChangeObject:atIndexPath:forChangeType:newIndexPath:)]){
        
        [self.delegate fetchedControllerDidChangeObject:anObject atIndexPath:indexPath forChangeType:type newIndexPath:newIndexPath];
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(fetchedControllerDidChangeContent)]){
        
        [self.delegate fetchedControllerDidChangeContent];
    }
    
}

@end
