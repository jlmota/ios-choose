//
//  CoreDataHelper.h
//  Mcdonalds
//
//  Created by Bruno Sousa on 6/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface CoreDataHelper : NSObject {
}

+ (NSArray *) getManagedObjects:(NSString *) tableName withPredicate:(NSPredicate *) predicate
                     andSortKey:(NSString*) sortKey inContext:(NSManagedObjectContext *) _managedObjectContext;

+ (NSMutableArray *) getObjectsForEntity:(NSString*)entityName withSortKey:(NSString*)sortKey 
                        andSortAscending:(BOOL)sortAscending 
                              andContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSArray *) getManagedObjects:(NSString *) tableName withPredicate:(NSPredicate *) predicate
                     andSortKey:(NSString*) sortKey isAscending:(BOOL) isAscending
                      inContext:(NSManagedObjectContext *) _managedObjectContext;

+ (NSMutableArray *) searchObjectsForEntity:(NSString*)entityName 
                              withPredicate:(NSPredicate *)predicate andSortKey:(NSString*)sortKey 
                           andSortAscending:(BOOL)sortAscending 
                                 andContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSMutableArray *)searchObjectsForEntity:(NSString*)entityName 
                            withPredicate:(NSPredicate *)predicate 
                               andSortKey:(NSString*)sortKey 
                         andSortAscending:(BOOL)sortAscending 
                                 andLimit:(int)limit 
                               andContext:(NSManagedObjectContext *)managedObjectContext;

+ (BOOL) deleteAllObjectsForEntity:(NSString*)entityName 
                        inContext:(NSManagedObjectContext *)managedObjectContext;

+ (BOOL) deleteAllObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate *) predicate
                       inContext:(NSManagedObjectContext *)managedObjectContext;

+ (NSManagedObject *) getManagedObject:(NSString *) tableName byId:(NSNumber *) _id 
                             inContext:(NSManagedObjectContext *) _managedObjectContext;

+ (BOOL) deleteManagedObject:(NSString *) tableName byId:(NSNumber *) _id 
                   inContext:(NSManagedObjectContext *) _managedObjectContext;

+ (BOOL) deleteManagedObject:(NSManagedObject *) managedObject
                   inContext:(NSManagedObjectContext *) _managedObjectContext;

+ (NSArray *) getManagedObjects:(NSString *) tableName withPredicate:(NSPredicate *) predicate 
                      inContext:(NSManagedObjectContext *) _managedObjectContext;

@end
