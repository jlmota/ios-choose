//
//  DataController.h
//  Cocacola
//
//  Created by Simão Seiça {ext} on 8/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface ContextSingleton : NSObject{

    
}

@property (nonatomic, retain, readonly) NSManagedObjectContext * managedObjectContext;

+ (ContextSingleton*) sharedInstance;

- (BOOL) saveContext;

@end
