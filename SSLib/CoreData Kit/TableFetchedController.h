//
//  CoreDataManager.h
//  MyDictionary
//
//  Created by Simao Seica on 3/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "FetchedController.h"

@protocol TableFetchedControllerProtocol;

@interface TableFetchedController : FetchedController {
    

    
}


- (NSInteger) getNumberOfSections;

- (NSInteger) getNumberOfRows;

- (NSInteger) getNumberOfRowsInSection:(NSInteger) section;

- (NSManagedObject *) getManagedObjectAtIndexPath:(NSIndexPath *)indexPath;

- (BOOL) deleteManagedObjectAtIndexPath:(NSIndexPath *)indexPath;

- (NSManagedObject *) getManagedObjectAtIndex:(NSInteger) index;

@end



