//
//  DataController.m
//  Cocacola
//
//  Created by Simão Seiça {ext} on 8/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ContextSingleton.h"
#import <UIKit/UIKit.h>
#import "CHAppDelegate.h"
#import "AppConstant.h"

@interface ContextSingleton()


@end

@implementation ContextSingleton

@synthesize managedObjectContext = __managedObjectContext;


#pragma mark - View lifecycle

+ (ContextSingleton *)sharedInstance {
    static ContextSingleton *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[ContextSingleton alloc] init];
    });
    
    return _instance;
}

#pragma mark - COREDATA

- (BOOL) saveContext
{
    
    @synchronized([ContextSingleton class])
	{
        NSError *error = nil;
        
        if (self.managedObjectContext != nil)
        {
            if ([self.managedObjectContext hasChanges] && ![self.managedObjectContext save:&error])
            {

                DLog(@"[ERROR] Unresolved error %@, %@", error, [error userInfo]);
                [self.managedObjectContext undo];
                
                return NO;
            }
        }else{

            DLog(@"[ERROR] Data not saved in the database.");
            return NO;
        }
        
        //[self.managedObjectContext reset];
        return YES;
    }
    
    return NO;
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
  NSPersistentStoreCoordinator *coordinator = [(CHAppDelegate *)
                                                 [[UIApplication sharedApplication] delegate] 
                                                 persistentStoreCoordinator];
  if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }  
    return __managedObjectContext;
}

@end
