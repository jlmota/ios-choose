//
//  PListManager.m
//  PoweradeIphone
//
//  Created by Simão Seiça on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PListManager.h"

@implementation PListManager

+ (NSDictionary *) getDicPListWithName:(NSString *) name{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    
    return [[NSDictionary alloc] initWithContentsOfFile:path];
}

+ (NSArray *) getArrayPListWithName:(NSString *) name{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"plist"];
    
    return [[NSArray alloc] initWithContentsOfFile:path];
}

@end
