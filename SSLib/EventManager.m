//
//  EventManager.m
//  LisbonWeek
//
//  Created by Simão Seiça on 9/27/12.
//  Copyright (c) 2012 Fullsix. All rights reserved.
//

#import "EventManager.h"
#import <EventKit/EventKit.h>
#import "UIDevice+Utils.h"

@implementation EventManager

+ (void) saveEventWithTitle:(NSString *) title startDate:(NSDate *) startDate andEndDate:(NSDate *) endDate
                   location:(NSString *) location andKey:(id) key{
    
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    
    if([UIDevice isiOSVersionHigherThan:@"6.0"]) {
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            
            if (granted){
                
                [self postEventWithTitle:title startDate:startDate andEndDate:endDate andLocation:location
                            inEventStore:eventStore andKey: key];
            }
        }];
    }else{
        
        [self postEventWithTitle:title startDate:startDate andEndDate:endDate andLocation:location
                    inEventStore:eventStore andKey: key];
    }
    
}

+ (void) postEventWithTitle:(NSString *) title startDate:(NSDate *) startDate andEndDate:(NSDate *) endDate
                andLocation:(NSString *) location inEventStore:(EKEventStore *) eventStore andKey:(id) key{
    
    NSError *err;
    EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
    event.title     = title;
    event.location  = location;
    event.startDate = startDate;
    event.endDate   = endDate;
    event.allDay = [startDate isEqual:endDate];
    
    [event setCalendar:[eventStore defaultCalendarForNewEvents]];
    
    [eventStore saveEvent:event
                     span:EKSpanThisEvent error:&err];
    
    if(!err){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kCALENDAR_NOTIFICATION object:@{kEVENT_DATA_KEY:key,
                                                 kEVENT_STATUS_KEY:[NSNumber numberWithBool:YES]}];
    }else{
        
        DLog(@"[ERROR]%@", err);
    }
}

@end
