//
//  main.m
//  Choose
//
//  Created by João Luís Mota on 08/11/14.
//  Copyright (c) 2014 João Luís Mota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CHAppDelegate class]));
    }
}
