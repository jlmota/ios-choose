//
//  ContextManager.h
//  Choose
//
//  Created by João Luís Mota on 11/23/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface ContextManager : NSObject

@property (nonatomic, retain, readonly) NSManagedObjectContext * managedObjectContext;

- (BOOL) saveContext;

- (BOOL) updateNewss:(NSArray *)jsonNewss;
- (void) updateNews:(NSArray *)jsonNews;
- (void) deleteNews:(NSString *) entityId;

- (BOOL) updateTeam:(NSArray *) jsonTeam;
- (void) updateTeamMember:(NSArray *)jsonTeamMember;
- (void) deleteTeamMember:(NSString *)entityId;

- (BOOL) updateVideos:(NSArray *) jsonVideos;
- (void) updateVideo:(NSArray *)jsonVideo;
- (void) delteVideo:(NSString *)entityId;

- (BOOL) updateCatalogues:(NSArray *) jsonCatalogues;
- (void) updateCatalogue:(NSArray *)jsonCatalogue;
- (BOOL) updateCatalogueMedias:(NSArray *) jsonCatalogueMedias;
- (void) deleteCatalogue:(NSString *)entityId;

@end