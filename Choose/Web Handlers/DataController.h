//
//  DataController.h
//  Choose
//
//  Created by João Luís Mota on 5/18/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataController : NSObject {
    
}

@property (nonatomic, retain, readonly) NSManagedObjectContext * managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel * managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator * persistentStoreCoordinator;

+ (DataController*)sharedInstance;

- (void)updateCHNews;
- (void)updateCHTeam;
- (void)updateCHVideos;
- (void)updateCHCatalogue;
- (void)updateCHCatalogueMedia:(NSNumber *)catalogueId;

@end
