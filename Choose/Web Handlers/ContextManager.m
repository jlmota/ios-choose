//
//  ContextManager.m
//  Choose
//
//  Created by João Luís Mota on 11/23/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import "ContextManager.h"
#import "ContextSingleton.h"
#import "UserDefaultsManager.h"
#import "UserDefaultsConstants.h"
#import "NotificationConstants.h"
#import "AppConstant.h"
#import "DatabaseConstants.h"
#import "ManagedObjectBuilder.h"
#import "WSConfig.h"
#import "CoreDataHelper.h"
#import "CHNewsBuilder.h"
#import "CHVideoBuilder.h"
#import "CHTeamBuilder.h"
#import "CHCatalogueBuilder.h"
#import "CHAppDelegate.h"
#import "CHCatalogueMediaBuilder.h"

@implementation ContextManager

@synthesize managedObjectContext = __managedObjectContext;

//==================================================================================================
#pragma mark - View lifecycle

- (id) init{
    self = [super init];
    if (self) {
        //Notification of changes
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter addObserver:self
                               selector:@selector(updateChanges:)
                                   name:NSManagedObjectContextDidSaveNotification
                                 object:[self managedObjectContext]];
    }
    return self;
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////NEWS//////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL) updateNewss:(NSArray *)jsonNewss
{
    if([[jsonNewss class] isSubclassOfClass:[NSArray class]]){
        DLog(@"[INFO]There are %lu news.", jsonNewss.count);
        
        for(NSArray *jsonNews in jsonNewss) {
            [self updateNews:jsonNews];
        }
        
        return [self saveContext];
    }
    
    return NO;
}


- (void) updateNews:(NSArray *)jsonNews
{
    [CHNewsBuilder updateManagedObject:jsonNews inContext:[self managedObjectContext]];
}

- (void) deleteNews:(NSString *)entityId
{
    [CoreDataHelper deleteAllObjectsForEntity:kCH_NEWS_TABLE
                                withPredicate:[NSPredicate predicateWithFormat:@"SELF.%@ == %@",kTABLE_ID_ATTR,entityId]
                                    inContext:[self managedObjectContext]];
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////VIDEOS////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL) updateVideos:(NSArray *)jsonVideos
{
    if([[jsonVideos class] isSubclassOfClass:[NSArray class]]){
        DLog(@"[INFO]There are %lu videos.", jsonVideos.count);
        
        for(NSArray *jsonVideo in jsonVideos) {
            [self updateVideo:jsonVideo];
        }
        
        return [self saveContext];
    }
    
    return NO;
}

- (void) updateVideo:(NSArray *)jsonVideo
{
    [CHVideoBuilder updateManagedObject:jsonVideo inContext:[self managedObjectContext]];
}

- (void) delteVideo:(NSString *)entityId
{
    [CoreDataHelper deleteAllObjectsForEntity:kCH_VIDEO_TABLE
                                withPredicate:[NSPredicate predicateWithFormat:@"SELF.%@ == %@",kTABLE_ID_ATTR,entityId]
                                    inContext:[self managedObjectContext]];
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////TEAM////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL) updateTeam:(NSArray *)jsonTeam
{
    if([[jsonTeam class] isSubclassOfClass:[NSArray class]]){
        DLog(@"[INFO]There are %lu TEAM MEMBERS.", jsonTeam.count);
        
        for(NSArray *jsonTeamMember in jsonTeam) {
            [self updateTeamMember:jsonTeamMember];
        }
        
        return [self saveContext];
    }
    
    return NO;
}


- (void) updateTeamMember:(NSArray *)jsonTeamMember
{
    [CHTeamBuilder updateManagedObject:jsonTeamMember inContext:[self managedObjectContext]];
}

- (void) deleteTeamMember:(NSString *)entityId
{
    [CoreDataHelper deleteAllObjectsForEntity:kCH_TEAM_TABLE
                                withPredicate:[NSPredicate predicateWithFormat:@"SELF.%@ == %@",kTABLE_ID_ATTR,entityId]
                                    inContext:[self managedObjectContext]];
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////CATALOGUE////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL) updateCatalogues:(NSArray *)jsonCatalogues
{
    if([[jsonCatalogues class] isSubclassOfClass:[NSArray class]]){
        DLog(@"[INFO]There are %lu catalogue.", jsonCatalogues.count);
        
        for(NSArray *jsonCatalogue in jsonCatalogues) {
            [self updateCatalogue:jsonCatalogue];
        }
        
        return [self saveContext];
    }
    
    return NO;
}

- (void) updateCatalogue:(NSArray *)jsonCatalogue
{
    [CHCatalogueBuilder updateManagedObject:jsonCatalogue inContext:[self managedObjectContext]];
}

- (BOOL) updateCatalogueMedias:(NSArray *)jsonCatalogueMedias
{
    if([[jsonCatalogueMedias class] isSubclassOfClass:[NSArray class]]){
        DLog(@"[INFO]There are %lu catalogueMedia.", jsonCatalogueMedias.count);
        for(NSDictionary *jsonCatalogueMedia in jsonCatalogueMedias) {
            [self updateCatalogueMedia:jsonCatalogueMedia];
        }
        
        return [self saveContext];
    }
    
    return NO;
}

- (void) updateCatalogueMedia:(NSDictionary *)jsonCatalogueMedia
{
    [CHCatalogueMediaBuilder updateManagedObject:jsonCatalogueMedia inContext:[self managedObjectContext]];
}

- (void) deleteCatalogue:(NSString *)entityId
{
    [CoreDataHelper deleteAllObjectsForEntity:kCH_CATALOGUE_TABLE
                                withPredicate:[NSPredicate predicateWithFormat:@"SELF.%@ == %@",kTABLE_ID_ATTR,entityId]
                                    inContext:[self managedObjectContext]];
}

//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
#pragma mark - CoreData

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [kCHAppDelegate persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

- (BOOL) saveContext{
    
    @synchronized([ContextManager class])
    {
        NSError *error = nil;
        
        if (self.managedObjectContext != nil){
            if ([self.managedObjectContext hasChanges]){
                if([self.managedObjectContext save:&error]){
                    
                    DLog(@"[INFO]Data saved successfully.");
                    return YES;
                }else{
                    
                    DLog(@"[ERROR]'%@'", [error userInfo]);
                    [self.managedObjectContext undo];
                    return NO;
                }
            }else{
                
                DLog(@"[INFO]Nothing to be saved.");
                return YES;
            }
        }
    }
    
    return NO;
}

#pragma mark - Notification Center

- (void) updateChanges:(NSNotification *)notification{
    
    //Get the context of the main Thread
    NSManagedObjectContext *mainContext = [[ContextSingleton sharedInstance] managedObjectContext];
    
    // Merge changes into the main context on the main thread
    [mainContext performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:)
                                  withObject:notification
                               waitUntilDone:YES];
}

@end
