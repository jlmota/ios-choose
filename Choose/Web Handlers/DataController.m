//
//  DataController.m
//  Choose
//
//  Created by João Luís Mota on 5/18/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import "DataController.h"
#import "UserDefaultsManager.h"
#import "ContextManager.h"
#import "WSConfig.h"
#import "DatabaseConstants.h"
#import "UIDevice+Utils.h"
#import "ThreadPool.h"
#import "AFHTTPRequestOperationManager.h"
#import "UserDefaultsManager.h"
#import "UserDefaultsConstants.h"
#import "NSDate+Utils.h"
#import "ConnectivitySingleton.h"
#import "KeyConstants.h"

@interface DataController()

@property(nonatomic, retain) ThreadPool *poolThread;
@property(nonatomic, strong) ContextManager * context;
@end

@implementation DataController

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

@synthesize poolThread;
@synthesize context;

#pragma mark -
#pragma mark - View lifecycle

+ (DataController *) sharedInstance {
    static DataController *_dataController = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _dataController = [[DataController alloc] init];
    });
    return _dataController;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        poolThread = [ThreadPool new];
        poolThread.maxThreadNumber = 2;
        
        [self managedObjectContext];
    }
    return self;
}


//==========================================================================================================================================
#pragma mark -
#pragma mark - News

- (void) updateCHNews
{    
    if([[ConnectivitySingleton sharedInstance] isOnline]){
        NSThread * thread = [[NSThread alloc] initWithTarget:self
                                                    selector:@selector(runNewsThread)
                                                      object:nil];
        thread.name = @"News Thread";
        [poolThread pushThread:thread];
    }
}

- (void) runNewsThread
{
    @autoreleasepool
    {
        ContextManager *dataManager = [[ContextManager alloc] init];
        
        NSString *timeStamp = [UserDefaultsManager getObjectForKey:kCH_NEWS_TIME_STAMP];
        NSString *updateURL = [NSString stringWithFormat:@"%@%@?choose=%@&lastDate=%@",
                               kCH_URL,
                               kCH_SERVICE,
                               kCH_NEWS_METHOD,
                               timeStamp ? timeStamp : kDEFAULT_TIME_STAMP];
        
        DLog(@"[INFO][NEWS]Calling '%@", updateURL);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET: updateURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSArray *JSON = (NSArray *)responseObject;
            
            if([JSON count] > 0){
                BOOL success = [dataManager updateNewss:JSON];
                
                if (success) {
                    success = [dataManager saveContext];
                    [self performSelectorOnMainThread:@selector(didEndNewsThread:) withObject:[NSNumber numberWithBool:success] waitUntilDone:NO];
                }
            } else {
                DLog(@"[INFO] No new NEWS");
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"Error: %@", error);
        }];
    }
    
}

-(void) didEndNewsThread:(NSNumber*)success
{
    if([success boolValue]) {
        [UserDefaultsManager saveObject:[NSDate getCurrentDate] withKey:kCH_NEWS_TIME_STAMP];
    }
}

//==========================================================================================================================================
#pragma mark -
#pragma mark - Videos

- (void) updateCHVideos
{
    if([[ConnectivitySingleton sharedInstance] isOnline]) {
        NSThread * thread = [[NSThread alloc] initWithTarget:self
                                                    selector:@selector(runVideosThread)
                                                      object:nil];
        thread.name = @"Videos Thread";
        [poolThread pushThread:thread];
    }
}

- (void) runVideosThread
{
    @autoreleasepool
    {
        ContextManager *dataManager = [[ContextManager alloc] init];
        
        NSString *timeStamp = [UserDefaultsManager getObjectForKey:kCH_VIDEO_TIME_STAMP];
        NSString *updateURL = [NSString stringWithFormat:@"%@%@?choose=%@&lastDate=%@",
                               kCH_URL,
                               kCH_SERVICE,
                               kCH_VIDEOS_METHOD,
                               timeStamp ? timeStamp : kDEFAULT_TIME_STAMP];
        
        DLog(@"[INFO][VIDEOS]Calling '%@", updateURL);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET: updateURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSArray *JSON = (NSArray *)responseObject;
             
             if([JSON count] > 0){
                BOOL success = [dataManager updateVideos:(NSArray *) JSON];
                
                if (success) {
                    success = [dataManager saveContext];
                    [self performSelectorOnMainThread:@selector(didEndVideosThread:) withObject:[NSNumber numberWithBool:success] waitUntilDone:NO];
                }
            } else {
                DLog(@"[INFO] NO new VIDEOS");
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"Error: %@", error);
        }];
    }
    
}

-(void) didEndVideosThread:(NSNumber*)success
{
    if([success boolValue]) {
        [UserDefaultsManager saveObject:[NSDate getCurrentDate] withKey:kCH_VIDEO_TIME_STAMP];
    }
}

//==========================================================================================================================================
#pragma mark -
#pragma mark - Team

- (void) updateCHTeam
{
    if([[ConnectivitySingleton sharedInstance] isOnline]){
        
        NSThread * thread = [[NSThread alloc] initWithTarget:self
                                                    selector:@selector(runTeamThread)
                                                      object:nil];
        thread.name = @"Team Thread";
        [poolThread pushThread:thread];
    }
}

- (void) runTeamThread
{
    @autoreleasepool
    {
        ContextManager *dataManager = [[ContextManager alloc] init];
        
        NSString *timeStamp = [UserDefaultsManager getObjectForKey:kCH_TEAM_TIME_STAMP];
        NSString *updateURL = [NSString stringWithFormat:@"%@%@?choose=%@&lastDate=%@",
                               kCH_URL,
                               kCH_SERVICE,
                               kCH_TEAM_METHOD,
                               timeStamp ? timeStamp : kDEFAULT_TIME_STAMP];
        
        DLog(@"[INFO][TEAM]Calling '%@", updateURL);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET: updateURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSArray *JSON = (NSArray *)responseObject;
             
             if([JSON count] > 0){
                BOOL success = [dataManager updateTeam:JSON];
                
                if (success) {
                    success = [dataManager saveContext];
                    [self performSelectorOnMainThread:@selector(didEndTeamThread:) withObject:[NSNumber numberWithBool:success] waitUntilDone:NO];
                }
            } else {
                DLog(@"[INFO] No new TEAM");
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"Error: %@", error);
        }];
        
    }
    
}

-(void) didEndTeamThread:(NSNumber*) success
{
    if([success boolValue]) {
        [UserDefaultsManager saveObject:[NSDate getCurrentDate] withKey:kCH_TEAM_TIME_STAMP];
    }
}



//==========================================================================================================================================
#pragma mark -
#pragma mark - Catalogue

- (void) updateCHCatalogue
{
    if([[ConnectivitySingleton sharedInstance] isOnline]){
        
        NSThread * thread = [[NSThread alloc] initWithTarget:self
                                                    selector:@selector(runCatalogueThread)
                                                      object:nil];
        thread.name = @"Catalogue Thread";
        [poolThread pushThread:thread];
    }
}

- (void) runCatalogueThread
{
    @autoreleasepool
    {
        ContextManager *dataManager = [[ContextManager alloc] init];
        
        NSString *timeStamp = [UserDefaultsManager getObjectForKey:kCH_CATALOGUE_METHOD];
        NSString *updateURL = [NSString stringWithFormat:@"%@%@?choose=%@&lastDate=%@",
                               kCH_URL,
                               kCH_SERVICE,
                               kCH_CATALOGUE_METHOD,
                               timeStamp ? timeStamp : kDEFAULT_TIME_STAMP];
        
        DLog(@"[INFO][CATALOGUE]Calling '%@", updateURL);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET: updateURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
            NSArray *JSON = (NSArray *)responseObject;
            
            if([JSON count] > 0){
                BOOL success = [dataManager updateCatalogues:JSON];
                
                if (success) {
                    [self performSelectorOnMainThread:@selector(didEndCatalogueThread:) withObject:[NSNumber numberWithBool:success] waitUntilDone:NO];
                } else {
                    DLog(@"[ERROR] Error saving CATALOGUE");
                }
            } else {
                DLog(@"[ERROR] Error retriving CATALOGUE");
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DLog(@"Error: %@", error);
        }];
    }
}

-(void) didEndCatalogueThread:(NSNumber*) success
{
    if([success boolValue]) {
        [UserDefaultsManager saveObject:[NSDate getCurrentDate] withKey:kCH_CATALOGUE_TIME_STAMP];
    }
}

- (void) updateCHCatalogueMedia:(NSNumber *)catalogueId
{
    if([[ConnectivitySingleton sharedInstance] isOnline]){
        
        NSThread * thread = [[NSThread alloc] initWithTarget:self
                                                    selector:@selector(runCatalogueMediaThread:)
                                                      object:catalogueId];
        thread.name = @"Catalogue Thread";
        [poolThread pushThread:thread];
    }
}

- (void)runCatalogueMediaThread:(NSString *)catalogueId
{
    @autoreleasepool
    {
        NSString *updateURL = [NSString stringWithFormat:@"%@%@?choose=%@&id=%@",
                               kCH_URL,
                               kCH_SERVICE,
                               kCH_CATALOGUE_METHOD,
                               catalogueId];
        
        DLog(@"[INFO][CATALOGUE]Calling '%@", updateURL);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET: updateURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSDictionary *JSON = (NSDictionary *)responseObject[0];
             
             if([JSON count] > 0){
                 NSArray *catalogueMediaArr = [JSON objectForKey:kCAT_MEDIA_KEY];
                 [self addMediaToCatalogue:catalogueMediaArr];
                 
             } else {
                 DLog(@"[ERROR] Error retriving CATALOGUE");
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             DLog(@"Error: %@", error);
         }];
    }
}

- (void) addMediaToCatalogue:(NSArray *)catalogueMediaArr
{
    ContextManager *dataManager = [[ContextManager alloc] init];
    BOOL success = [dataManager updateCatalogueMedias:catalogueMediaArr];
    
    if (success) {
        [self performSelectorOnMainThread:@selector(didEndCatalogueThread:) withObject:[NSNumber numberWithBool:success] waitUntilDone:NO];
    } else {
        DLog(@"[ERROR] Error saving CATALOGUE MEDIAS");
    }
}

-(void) didEndCatalogueMediaThread:(NSNumber*) success
{
    if([success boolValue]) {
        [UserDefaultsManager saveObject:[NSDate getCurrentDate] withKey:kCH_CATALOGUE_TIME_STAMP];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@end
