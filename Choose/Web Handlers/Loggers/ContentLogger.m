//
//  ContentLogger.m
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "ContentLogger.h"
#import "HTProgressHUD.h"

@implementation ContentLogger


- (BOOL) updateAllContent{
    
    DLog(@"[WARNING]Should not be here.");
    return NO;
}

- (BOOL) createContent{
    
    DLog(@"[WARNING]Should not be here.");
    return NO;
}

- (BOOL) editContent{
    
    DLog(@"[WARNING]Should not be here.");
    return NO;
}

- (BOOL) deleteContent{
    
    DLog(@"[WARNING]Should not be here.");
    return NO;
}

#pragma mark - HUD display
//TODO: progress
- (void) showProgressWithCurrent:(int)current andTotal:(int)total andTitle:(NSString *)title{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        float progress = (float)current / (float)total;
        NSString *pre = @"A atualizar...";
        
        NSString *message = [NSString stringWithFormat: @"%@\n%@\n%d / %d", pre, title, current, total];
        
        //[SVProgressHUD showProgress:progress status:message];
    });
}

@end