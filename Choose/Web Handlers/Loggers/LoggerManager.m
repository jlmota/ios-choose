//
//  LoggerManager.m
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "LoggerManager.h"
#import "ThreadPool.h"
#import "NewsLogger.h"
#import "VideoLogger.h"
#import "TeamLogger.h"
#import "CatalogueLogger.h"
#import "HTProgressHUD.h"

@interface LoggerManager ()

@property(nonatomic, retain) ThreadPool *poolThread;

@end

@implementation LoggerManager

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize poolThread;

#pragma mark - View lifecycle

+ (LoggerManager *) sharedInstance {
    static LoggerManager *_intance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _intance = [[LoggerManager alloc] init];
    });
    return _intance;
}

- (id)init
{
    self = [super init];
    if (self) {
        poolThread = [ThreadPool new];
        poolThread.maxThreadNumber = 2;
        poolThread.delegate = self;
        
        [self managedObjectContext];
    }
    return self;
}

- (BOOL) updateContent:(ContentType) contentType{
    
    switch (contentType) {
        case NEWS_TYPE:
        {
            NSThread *thread = [[NSThread alloc] initWithTarget:self
                                                       selector:@selector(updateNews)
                                                         object:nil];
            thread.name = @"News Thread";
            [poolThread pushThread:thread];
        }   break;
        case VIDEO_TYPE:
        {
            NSThread *thread = [[NSThread alloc] initWithTarget:self
                                                       selector:@selector(updateVideos)
                                                         object:nil];
            thread.name = @"Videos Thread";
            [poolThread pushThread:thread];
        }   break;
        case TEAM_TYPE:
        {
            NSThread *thread = [[NSThread alloc] initWithTarget:self
                                                       selector:@selector(updateTeam)
                                                         object:nil];
            thread.name = @"Team Thread";
            [poolThread pushThread:thread];
        }   break;
        case CATALOGUE_TYPE:
        {
            NSThread *thread = [[NSThread alloc] initWithTarget:self
                                                       selector:@selector(updateCatalogue)
                                                         object:nil];
            thread.name = @"Catalogue Thread";
            [poolThread pushThread:thread];
        }   break;
    }
    
    return YES;
}

- (void) updateNews{
    @autoreleasepool {
        DLog(@"Running  %@", [NSThread currentThread].name);
        
        NewstLogger *logger = [NewstLogger new];
        [logger updateAllContent];
    }
}

- (void) updateVideos{
    @autoreleasepool {
        DLog(@"Running  %@", [NSThread currentThread].name);
        
        VideoLogger *logger = [VideoLogger new];
        [logger updateAllContent];
    }
}

- (void) updateTeam{
    
    @autoreleasepool {
        DLog(@"Running  %@", [NSThread currentThread].name);
        
        TeamLogger *logger = [TeamLogger new];
        [logger updateAllContent];
    }
}

- (void) updateCatalogue{
    
    @autoreleasepool {
        DLog(@"Running  %@", [NSThread currentThread].name);
        
        CatalogueLogger *logger = [CatalogueLogger new];
        [logger updateAllContent];
    }
}

- (void) didEndLoggerThread:(NSNumber*) success{
    
    
}

-(void) dismissDialog{
    
   
}

- (void)poolCleared:(ThreadPool *)threadPool{

    dispatch_async(dispatch_get_main_queue(), ^{
        //TODO: Progress
        //[HTProgressHUD hide];
    });
}

@end
