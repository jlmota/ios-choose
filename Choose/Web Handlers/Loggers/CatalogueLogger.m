//
//  CatalogueLogger.h
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CatalogueLogger.h"
#import "WSConfig.h"
#import "ServerManager.h"
#import "UserDefaultsManager.h"
#import "UserDefaultsConstants.h"
#import "DatabaseConstants.h"
#import "JSONKit.h"
#import "ContextManager.h"
#import "UserDefaultsConstants.h"
#import "NotificationConstants.h"
#import "KeyConstants.h"
#import "NSDate+Utils.h"
#import "CoreDataHelper.h"
#import "AFHTTPRequestOperationManager.h"

@implementation CatalogueLogger

- (BOOL) updateAllContent{
    
    NSString * timeStamp = [UserDefaultsManager getObjectForKey:kCH_CATALOGUE_TIME_STAMP];
    if([timeStamp length] == 0){
        
        timeStamp = kDEFAULT_TIME_STAMP;
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?lastDate=%@&categ=%@&cacheBuster=%@", kCH_URL, kCH_SERVICE,
                                       timeStamp, kCH_CATALOGUES_METHOD, [NSDate getCurrentDate]]];
    
    DLog(@"[INFO][REQUEST][CATALOGUE]Calling '%@'", url);
    NSString * responseJson = [ServerManager connectServerWithUrl:url];
    //DLog(@"[INFO][RESPONSE]'%@'", responseJson);
    
    if([responseJson length] != 0 && ![responseJson isEqualToString:kNULL]){
        
        NSString *lastLog = @"";
        BOOL success = YES;
        NSArray *actions = [responseJson objectFromJSONString];
        
        ContextManager *context = [[ContextManager alloc] init];
        
        for(NSDictionary *jsonDic in actions){
            
            NSString *log_action = [jsonDic objectForKey:kLOG_ACTION_KEY];
            NSString *log_category_id = [jsonDic objectForKey:kLOG_CATEGORY_ID_KEY];
            lastLog = [jsonDic objectForKey:kLOG_DATE_KEY];
            
            DLog(@"[CATALOGUELOGGER] %@ - Entityid:%@", log_action, log_category_id);
            
            if([log_action isEqualToString:kEDIT_TAG] || [log_action isEqualToString:kADD_TAG]){
                
                [self updateCataglogue:log_category_id inContext:context];
                
            }else if ([log_action isEqualToString:kDELETE_TAG]){
                
                [context deleteCatalogue:log_category_id];
                
            }
            
            success &= [context saveContext];
        }
        
        if(success){
            DLog(@"[INFO] Catalogue saved successfully.");
            
            if([lastLog length] > 0){
                lastLog = [lastLog stringByReplacingOccurrencesOfString:@" " withString:@"T"];
                [UserDefaultsManager saveObject:lastLog withKey:kCH_CATALOGUE_TIME_STAMP];
            }
            
            return YES;
        }
    }
    
    return NO;
}

- (void) updateCataglogue:(NSString *) entityId inContext:(ContextManager *)context
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?choose=%@&id=%@&cacheBuster=%@", kCH_URL, kCH_SERVICE, kCH_CATALOGUE_METHOD, entityId, [NSDate getCurrentDate]]];
    
    DLog(@"[INFO][REQUEST]Calling '%@'", url);
    NSString * responseJson = [ServerManager connectServerWithUrl:url];
    DLog(@"[INFO][RESPONSE]'%@'", responseJson);
    
    if([responseJson length] != 0 && ![responseJson isEqualToString:kNULL]){
        
        [context updateCatalogues:[responseJson objectFromJSONString]];
        
    }
}

@end
