//
//  ContentLogger.h
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentLogger : NSObject

- (BOOL) updateAllContent;

- (BOOL) createContent;

- (BOOL) editContent;

- (BOOL) deleteContent;

- (void) showProgressWithCurrent:(int)current andTotal:(int)total andTitle:(NSString *)title;

@end
