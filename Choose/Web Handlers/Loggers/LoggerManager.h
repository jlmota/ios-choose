//
//  LoggerManager.h
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadPool.h"

typedef enum {
    NEWS_TYPE,
    VIDEO_TYPE,
    TEAM_TYPE,
    CATALOGUE_TYPE
} ContentType;

@interface LoggerManager : NSObject<ThreadPoolDelegate>


@property (nonatomic, retain, readonly) NSManagedObjectContext * managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel * managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator * persistentStoreCoordinator;

+ (LoggerManager *) sharedInstance;

- (BOOL) updateContent:(ContentType) contentType;

@end
