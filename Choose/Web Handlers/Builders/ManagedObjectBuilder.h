//
//  NSManagedObjectBuilder.h
//  LisbonWeek
//
//  Created by Simão Seiça on 9/27/12.
//  Copyright (c) 2012 Fullsix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ManagedObjectBuilder : NSObject

+ (NSManagedObject *)updateManagedObject:(id) eventDic inContext:(NSManagedObjectContext *) context;

+ (NSManagedObject *)updateManagedObject:(id) currencyDic withData:(NSDictionary *) values inContext:(NSManagedObjectContext *) context;

@end
