//
//  CHVideoBuilder.m
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHVideoBuilder.h"
#import "DatabaseConstants.h"
#import "ServerManager.h"
#import "WSConfig.h"
#import "CHQueryHelper.h"
#import "CHVideo.h"
#import "KeyConstants.h"
#import "NSDate+Utils.h"

@implementation CHVideoBuilder

+ (CHVideo *) updateManagedObject:(NSDictionary *)videoDic inContext:(NSManagedObjectContext *)context
{    
    @try
    {
        CHVideo *video = [CHQueryHelper getVideoFromDataBase:[videoDic objectForKey:kVIDEO_ID_KEY] inContext:context];
        
        if (!video) {
            video = (CHVideo *)[NSEntityDescription insertNewObjectForEntityForName:kCH_VIDEO_TABLE
                                                           inManagedObjectContext:context];
            
            DLog(@"[INFO][VIDEO]Adding Video with id '%@'.", video.tableID);
            video.tableID = [NSNumber numberWithInteger:[[videoDic objectForKey:kVIDEO_ID_KEY] integerValue]];
        }else{
            DLog(@"[INFO][VIDEO]Updating Video with id %@", [videoDic objectForKey:kTEAM_ID_KEY]);
        }
        
        video.name = [videoDic objectForKey:kVIDEO_TITLE_KEY];
        video.image = [videoDic objectForKey:kVIDEO_IMAGE_KEY];
        video.video = [videoDic objectForKey:kVIDEO_EMBED_KEY];
        video.date = [NSDate getDateByString: [videoDic objectForKey:kVIDEO_INSERT_DATE_KEY]];
        
        return video;
    }
    
    @catch (NSException *exception) {
        return nil;
    }
}

@end
