//
//  CHNewsBuilder.m
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHCatalogueBuilder.h"
#import "CHCatalogueMediaBuilder.h"
#import "DatabaseConstants.h"
#import "ServerManager.h"
#import "WSConfig.h"
#import "CHQueryHelper.h"
#import "CHCatalogue.h"
#import "KeyConstants.h"
#import "NSDate+Utils.h"

@implementation CHCatalogueBuilder

+ (CHCatalogue *) updateManagedObject:(NSDictionary *) catalogueDic inContext:(NSManagedObjectContext *) context
{
    @try
    {
        NSNumber *catalogueID = [NSNumber numberWithInt:[[catalogueDic objectForKey:kCAT_ID_KEY] intValue]];
        
        CHCatalogue *catalogue = [CHQueryHelper getCatalogueFromDataBase:catalogueID inContext:context];
        if (!catalogue) {
            catalogue = (CHCatalogue *)[NSEntityDescription insertNewObjectForEntityForName:kCH_CATALOGUE_TABLE
                                                                     inManagedObjectContext:context];
            catalogue.tableID = catalogueID;
            
            DLog(@"[INFO][CATALOGUE]Adding Catalogue with id '%@'.", [catalogueDic objectForKey:kCAT_ID_KEY]);
        } else {
            DLog(@"[INFO][CATALOGUE]Updating Catalogue with id %@", [catalogueDic objectForKey:kCAT_ID_KEY]);
        }
        
        catalogue.name = [catalogueDic objectForKey:kCAT_NAME_KEY];
        catalogue.image = [catalogueDic objectForKey:kCAT_IMAGE_KEY];
        catalogue.date = [NSDate getDateByString: [catalogueDic objectForKey:kCAT_INSERT_DATE_KEY]];
        
        return catalogue;
    }
    
    @catch (NSException *exception) {
        return nil;
    }
}

@end
