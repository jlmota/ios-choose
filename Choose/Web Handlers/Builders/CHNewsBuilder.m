//
//  CHNewsBuilder.m
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHNewsBuilder.h"
#import "DatabaseConstants.h"
#import "ServerManager.h"
#import "WSConfig.h"
#import "CHQueryHelper.h"
#import "CHNews.h"
#import "KeyConstants.h"
#import "DatabaseConstants.h"
#import "NSDate+Utils.h"

@implementation CHNewsBuilder

+ (CHNews *) updateManagedObject:(NSDictionary *)newsDic inContext:(NSManagedObjectContext *)context
{
    @try {
        
        CHNews *news = [CHQueryHelper getNewsFromDataBase:[newsDic objectForKey:kNEWS_ID_KEY] inContext:context];
        
        if (!news) {
            news = (CHNews *)[NSEntityDescription insertNewObjectForEntityForName:kCH_NEWS_TABLE
                                                           inManagedObjectContext:context];
            news.tableID = [NSNumber numberWithInteger:[[newsDic objectForKey:kNEWS_ID_KEY] integerValue]];
            
            DLog(@"[INFO][NEWS]Adding News with id %@", [newsDic objectForKey:kNEWS_ID_KEY]);
        }else {
            DLog(@"[INFO][NEWS]Updating News with id '%@'.", news.tableID);
        }
        
        news.name = [newsDic objectForKey:kNEWS_TITLE_KEY];
        news.text = [newsDic objectForKey:kNEWS_TEXT_KEY];
        news.image = [newsDic objectForKey:kNEWS_IMAGE_KEY];
        news.date = [NSDate getDateByString: [newsDic objectForKey:kNEWS_INSERT_DATE_KEY]];
        
        return news;
    }
    
    @catch (NSException *exception) {
        return nil;
    }
}

@end
