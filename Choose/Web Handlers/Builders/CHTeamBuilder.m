//
//  CHTeamBuilder.m
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHTeamBuilder.h"
#import "DatabaseConstants.h"
#import "ServerManager.h"
#import "WSConfig.h"
#import "CHQueryHelper.h"
#import "CHTeam.h"
#import "NSDate+Utils.h"
#import "KeyConstants.h"

@implementation CHTeamBuilder

+ (CHTeam *) updateManagedObject:(NSDictionary *)teamDic inContext:(NSManagedObjectContext *)context
{
    @try
    {
        CHTeam *team = [CHQueryHelper getTeamFromDataBase:[teamDic objectForKey:kTEAM_ID_KEY] inContext:context];
        
        if (!team) {
            team = (CHTeam *)[NSEntityDescription insertNewObjectForEntityForName:kCH_TEAM_TABLE
                                                           inManagedObjectContext:context];
            
            DLog(@"[INFO][TEAM]Adding Team Member with id '%@'.", team.tableID);
            team.tableID = [NSNumber numberWithInteger:[[teamDic objectForKey:kTEAM_ID_KEY] integerValue]];
        }else{
            DLog(@"[INFO][TEAM]Updating Team Member with id %@", [teamDic objectForKey:kTEAM_ID_KEY]);
        }
        
        team.name = [teamDic objectForKey:kTEAM_NAME_KEY];
        team.image = [teamDic objectForKey:kTEAM_IMAGE_KEY];
        team.text = [teamDic objectForKey:kTEAM_TEXT_KEY];
        team.date = [NSDate getDateByString: [teamDic objectForKey:kTEAM_INSERT_DATE_KEY]];
        
        return team;
    }
    
    @catch (NSException *exception) {
        return nil;
    }
}

@end
