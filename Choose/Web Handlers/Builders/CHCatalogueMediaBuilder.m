//
//  CHNewsBuilder.m
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHCatalogueMediaBuilder.h"
#import "DatabaseConstants.h"
#import "ServerManager.h"
#import "WSConfig.h"
#import "CHQueryHelper.h"
#import "CHCatalogueMedia.h"
#import "KeyConstants.h"

@implementation CHCatalogueMediaBuilder

+ (CHCatalogueMedia *) updateManagedObject:(NSDictionary *) catalogueMediaDic inContext:(NSManagedObjectContext *) context
{
    @try
    {
        CHCatalogueMedia *catalogueMedia = [CHQueryHelper getCatalogueMediaFromDataBase:[catalogueMediaDic objectForKey:kCAT_MEDIA_ID_KEY] inContext:context];
        
        if (!catalogueMedia) {
            catalogueMedia = (CHCatalogueMedia *)[NSEntityDescription insertNewObjectForEntityForName:kCH_CATALOGUE_MEDIA_TABLE
                                                                               inManagedObjectContext:context];
            
            DLog(@"[INFO][CATALOGUE_MEDIA]Adding new media for Catalogue id %@", [catalogueMediaDic objectForKey:kCAT_ID_KEY]);
            catalogueMedia.tableID = [NSNumber numberWithInt:[[catalogueMediaDic objectForKey:kCAT_MEDIA_ID_KEY] intValue]];
        } else {
            
            DLog(@"[INFO][CATALOGUE_MEDIA]Updating new media for Catalogue id %@", [catalogueMediaDic objectForKey:kCAT_ID_KEY]);
        }
        
        
        NSDateFormatter * dateFormater = [[NSDateFormatter alloc]init];
        [dateFormater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate * parsedDate = [dateFormater dateFromString:[catalogueMediaDic objectForKey:kCAT_MEDIA_DATE_KEY]];
        
        catalogueMedia.image = [catalogueMediaDic objectForKey:kCAT_MEDIA_IMAGE_KEY];
        catalogueMedia.date = parsedDate;
        catalogueMedia.catID = [NSNumber numberWithInt:[[catalogueMediaDic objectForKey:kCAT_ID_KEY] intValue]];
        
        return catalogueMedia;
    }
    
    @catch (NSException *exception) {
        return nil;
    }
}

@end
