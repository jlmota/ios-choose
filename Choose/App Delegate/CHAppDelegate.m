//
//  AppDelegate.m
//  Choose
//
//  Created by João Luís Mota on 08/11/14.
//  Copyright (c) 2014 João Luís Mota. All rights reserved.
//

#import "CHAppDelegate.h"
#import "MMDrawerController.h"
#import "CHNewsViewController.h"
#import "CHMenuViewController.h"
#import "ConnectivitySingleton.h"
#import "UserDefaultsManager.h"
#import "AppConstant.h"
#import "DatabaseConstants.h"
#import "KeyConstants.h"
#import "UIAlertView+Utils.h"
#import <sys/xattr.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "GAIDictionaryBuilder.h"
#import "GAConstants.h"

@interface CHAppDelegate ()

@property (nonatomic, retain) id<GAITracker>    tracker;

@end

@implementation CHAppDelegate

@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[CrashlyticsKit]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[ConnectivitySingleton sharedInstance] isOnline];
    [self initGoogleAnalytics];
    
    // Override point for customization after application launch.
    UIViewController * menuViewController = [[CHMenuViewController alloc] init];
    UIViewController * newsViewController = [[CHNewsViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newsViewController];
    
    MMDrawerController * drawerController = [[MMDrawerController alloc] initWithCenterViewController:navigationController
                                                                            leftDrawerViewController:menuViewController];
    
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = drawerController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Core Data

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:kCH_DATABASE_MODEL withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *) persistentStoreCoordinator
{
    
    if (__persistentStoreCoordinator != nil){
        
        return __persistentStoreCoordinator;
    }
    
    kIS_LOGGING ? [self populateDatabase] : nil;
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString
                                                                                         stringWithFormat:@"%@.sqlite", kCH_DATABASE_MODEL]];
    //this is not necesssary to the NSLibraryDirectory but is to assure Apple accept
    
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    // Allow inferred migration from the original version of the application.
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES],
                             NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES],
                             NSInferMappingModelAutomaticallyOption, nil];
    
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
    {
        
        DLog(@"[ERROR]Error creating the database %@, %@", error, [error userInfo]);
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        __persistentStoreCoordinator = nil;
        return [self persistentStoreCoordinator];
    }
    
    return __persistentStoreCoordinator;
}

- (void) populateDatabase{
    
    NSString * dbpath = [self getDBPath];
    NSFileManager *filemanager = [[NSFileManager alloc] init];
    
    
    if (![filemanager fileExistsAtPath:dbpath] ||
        [[UserDefaultsManager getObjectForKey:kDB_VERSION_KEY] compare:kAPP_VERSION] == NSOrderedAscending){
        
        [self copyDBTo:dbpath withManager:filemanager];
    }
}

- (void) copyDBTo:(NSString *) dbpath withManager:(NSFileManager *) filemanager{
    // try to copy from default, if we have it
    NSString * defaultDBPath = [[NSBundle mainBundle] pathForResource:kCH_DATABASE_MODEL ofType:@"sqlite"];
    
    if ([filemanager fileExistsAtPath:defaultDBPath]) {
        [filemanager copyItemAtPath:defaultDBPath toPath:dbpath error:NULL];
        
        DLog(@"[INFO]Database populated correctly.");
        [UserDefaultsManager saveObject:kAPP_VERSION withKey:kDB_VERSION_KEY];
    }
}

- (NSString *) getDBPath {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", kCH_DATABASE_MODEL]];
}


/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                         inDomains:NSUserDomainMask] lastObject];
    
    //NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSLibraryDirectory
    //                                                     inDomains:NSUserDomainMask] lastObject];
    [self addSkipBackupAttributeToItemAtURL:url];
    
    return url;
}

#pragma mark - Private Methods

- (BOOL) addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    
    const char* filePath = [[URL path] fileSystemRepresentation];
    const char* attrName = "com.apple.MobileBackup";
    if (&NSURLIsExcludedFromBackupKey == nil) {
        // iOS 5.0.1 and lower
        u_int8_t attrValue = 1;
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
        
    }
    else {
        // First try and remove the extended attribute if it is present
        int result = getxattr(filePath, attrName, NULL, sizeof(u_int8_t), 0, 0);
        if (result != -1) {
            // The attribute exists, we need to remove it
            int removeResult = removexattr(filePath, attrName, 0);
            if (removeResult == 0) {
                DLog(@"Removed extended attribute on file %@", URL);
            }
        }
        
        // Set the new key
        NSError *error = nil;
        [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        return error == nil;
    }
}


#pragma mark - Public Methods

- (BOOL) isConnected{
    
    if(![[ConnectivitySingleton sharedInstance] isOnline]){
        
        NSString *title = kINTERNET_WARNING_TITLE;
        NSString *message = kINTERNET_WARNING_MESSAGE;
        [UIAlertView showAlertScreenWithTitle:title andMessage:message];
        
        return NO;
    }
    
    return YES;
}

#pragma mark -
#pragma mark - GoogleAnalytics

- (void)initGoogleAnalytics
{
    [GAI sharedInstance].dispatchInterval = 120;
    //automatically send uncaught exceptions to Google Analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelWarning];
    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:kGOOGLE_ANALITICS_TRAKER_ID];
}

- (void)sendEvent:(NSString *)category forAction:(NSString *)action withLabel:(NSString *)label andValue:(NSNumber *) value
{
    [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:category    // Event category (required)
                                                                                      action:action  // Event action (required)
                                                                                       label:label          // Event label
                                                                                       value:value] build]];    // Event value
}


@end
