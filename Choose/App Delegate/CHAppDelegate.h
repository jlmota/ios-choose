//
//  AppDelegate.h
//  Choose
//
//  Created by João Luís Mota on 08/11/14.
//  Copyright (c) 2014 João Luís Mota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong, readonly) NSManagedObjectModel * managedObjectModel;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator * persistentStoreCoordinator;


- (void)sendEvent:(NSString *)category forAction:(NSString *)action withLabel:(NSString *)label andValue:(NSNumber *) value;

@end

