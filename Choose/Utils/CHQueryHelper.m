//
//  CHQueryHelper.m
//  Choose
//
//  Created by João Luís Mota on 8/26/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHQueryHelper.h"
#import "DatabaseConstants.h"


@implementation CHQueryHelper

+ (CHNews *) getNewsFromDataBase:(NSNumber *) tableId inContext:(NSManagedObjectContext *) context{
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@", kTABLE_ID_ATTR, tableId];
    
    NSArray *objects = [CoreDataHelper getManagedObjects:kCH_NEWS_TABLE
                                           withPredicate:predicate
                                               inContext:context];
    
    return objects && [objects count] > 0 ? [objects objectAtIndex: 0] : nil;
}

+ (CHVideo *) getVideoFromDataBase:(NSNumber *)tableId inContext:(NSManagedObjectContext *)context {
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@", kTABLE_ID_ATTR, tableId];
    
    NSArray *objects = [CoreDataHelper getManagedObjects:kCH_VIDEO_TABLE
                                           withPredicate:predicate
                                               inContext:context];
    
    return objects && [objects count] > 0 ? [objects objectAtIndex: 0] : nil;
}

+ (CHTeam *) getTeamFromDataBase:(NSNumber *)tableId inContext:(NSManagedObjectContext *)context {
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@", kTABLE_ID_ATTR, tableId];
    
    NSArray *objects = [CoreDataHelper getManagedObjects:kCH_TEAM_TABLE
                                           withPredicate:predicate
                                               inContext:context];
    
    return objects && [objects count] > 0 ? [objects objectAtIndex: 0] : nil;
}

+ (CHCatalogue *) getCatalogueFromDataBase:(NSNumber *)tableId inContext:(NSManagedObjectContext *)context {
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@", kTABLE_ID_ATTR, tableId];
    
    NSArray *objects = [CoreDataHelper getManagedObjects:kCH_CATALOGUE_TABLE
                                           withPredicate:predicate
                                               inContext:context];
    
    return objects && [objects count] > 0 ? [objects objectAtIndex: 0] : nil;
}

+ (CHCatalogueMedia *) getCatalogueMediaFromDataBase:(NSNumber *)tableId inContext:(NSManagedObjectContext *)context {
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.%@ == %@", kTABLE_ID_ATTR, tableId];
    
    NSArray *objects = [CoreDataHelper getManagedObjects:kCH_CATALOGUE_MEDIA_TABLE
                                           withPredicate:predicate
                                               inContext:context];
    
    return objects && [objects count] > 0 ? [objects objectAtIndex: 0] : nil;
}

@end
