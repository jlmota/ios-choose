//
//  CHQueryHelper.h
//  Choose
//
//  Created by João Luís Mota on 10/22/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataHelper.h"
#import "CHNews.h"
#import "CHVideo.h"
#import "CHTeam.h"
#import "CHCatalogue.h"
#import "CHCatalogueMedia.h"

@interface CHQueryHelper : CoreDataHelper

+ (CHNews *) getNewsFromDataBase:(NSNumber *) tableId inContext:(NSManagedObjectContext *) context;
+ (CHVideo *) getVideoFromDataBase:(NSNumber *) tableId inContext:(NSManagedObjectContext *) context;
+ (CHTeam *) getTeamFromDataBase:(NSNumber *) tableId inContext:(NSManagedObjectContext *) context;
+ (CHCatalogue *) getCatalogueFromDataBase:(NSNumber *) tableId inContext:(NSManagedObjectContext *) context;
+ (CHCatalogueMedia *) getCatalogueMediaFromDataBase:(NSNumber *) tableId inContext:(NSManagedObjectContext *) context;


@end
