//
//  ChooseTabBarIcon.m
//  Choose
//
//  Created by João Luís Mota on 5/18/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import "ChooseTabBarIcon.h"

@implementation ChooseTabBarIcon

@synthesize iconImage;

- (void) awakeFromNib {
    self.backgroundColor = [UIColor blackColor];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
   /*
    [self setBackgroundColor:
     [UIColor colorWithPatternImage:[UIImage imageNamed:@"footer_bt_over.png"]]];
    */
    [_delegate didTouch:self.tag - 1];
}

@end
