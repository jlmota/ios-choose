//
//  ChooseTabBarIcon.h
//  Choose
//
//  Created by João Luís Mota on 5/18/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseTabBarIconProtocol.h"

@interface ChooseTabBarIcon : UIView

@property (nonatomic, retain) IBOutlet UIImageView * iconImage;
@property ( nonatomic, retain ) id<ChooseTabBarIconProtocol> delegate;

@end
