//
//  TabIconProtocol.h
//  Choose
//
//  Created by João Luís Mota on 5/19/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChooseTabBarIconProtocol <NSObject>

- (void) didTouch:(NSUInteger) index;

@end
