//
//  CHTeamContentView.m
//  Choose
//
//  Created by João Luís Mota on 4/13/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import "TeamContentView.h"
#import "UIImageView+AFNetworking.h"
#import "WSConfig.h"

@interface TeamContentView()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end

@implementation TeamContentView

@synthesize imageView;

#pragma mark -
#pragma mark - PublicMethods

- (void)setContentImageView:(NSString *)imageName
{
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", kCH_URL, kCH_TEAM_METHOD, imageName]]];
    UIImage *image = [UIImage imageWithData:imageData];
    
    self.imageView.height = image.size.height / 2;
    self.imageView.image = image;
    self.imageView.backgroundColor = [UIColor clearColor];
    
    self.height = self.imageView.height;
    
    self.backgroundColor = [UIColor clearColor];
}

@end
