//
//  CHTeamHeaderView.m
//  Choose
//
//  Created by João Luís Mota on 4/13/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import "TeamHeaderView.h"
#import "UIImageView+AFNetworking.h"
#import "WSConfig.h"

@interface TeamHeaderView()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end

@implementation TeamHeaderView

@synthesize imageView;

#pragma mark -
#pragma mark - PublicMethods

- (void)setHeaderImageView:(NSString *)imageName
{
    
    [self.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", kCH_URL, kCH_TEAM_METHOD, imageName]]
                   placeholderImage:[UIImage imageNamed:@"news_background"]];
}

@end
