//
//  CHTeamHeaderView.h
//  Choose
//
//  Created by João Luís Mota on 4/13/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamHeaderView : UIView

- (void)setHeaderImageView:(NSString *)imageName;

@end
