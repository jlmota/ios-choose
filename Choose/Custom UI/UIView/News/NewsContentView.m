//
//  NewsContentView.m
//  Choose
//
//  Created by João Luís Mota on 2/9/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import "NewsContentView.h"
#import "UIFontConstants.h"

@interface NewsContentView()

@property (nonatomic, weak) IBOutlet UITextView *newsTextView;

@end

@implementation NewsContentView

@synthesize newsTextView;
@synthesize newsTitleLabel;

#pragma mark -
#pragma mark - ViewLifecyle

- (void)awakeFromNib {
    self.newsTitleLabel.font = kFONT_BEBAS_NEUE(16);
    self.newsTextView.font = kFONT_BEBAS_NEUE(14);
    //self.newsTextView.scrollEnabled = NO;    
}

#pragma mark -
#pragma mark - PublicMethods

- (void) setTextViewText:(NSString *)text {
    self.newsTextView.text = [NSString stringWithFormat:@"%@", text];
    //[self.newsTextView sizeToFit];
    
    self.height = self.newsTextView.y + self.newsTextView.height + 20;
}

@end
