//
//  NewsHeaderView.m
//  Choose
//
//  Created by João Luís Mota on 6/22/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import "NewsHeaderView.h"
#import "UIImageView+AFNetworking.h"
#import "WSConfig.h"

@interface NewsHeaderView()

@property (nonatomic, weak) IBOutlet UIImageView * imageView;

@end

@implementation NewsHeaderView

@synthesize imageView;

#pragma mark - 
#pragma mark - PublicMethods

- (void)setHeaderImageView:(NSString *)imageName
{
    [self.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", kCH_URL, kCH_NEWS_METHOD, imageName]]
                   placeholderImage:[UIImage imageNamed:@"news_background"]];
}

@end
