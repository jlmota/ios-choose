//
//  NewsHeaderView.h
//  Choose
//
//  Created by João Luís Mota on 6/22/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsHeaderView : UIView

@property (nonatomic, strong) NSString *title;

- (void)setHeaderImageView:(NSString *)imageName;

@end
