//
//  NewsContentView.h
//  Choose
//
//  Created by João Luís Mota on 2/9/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsContentView : UIView

@property (nonatomic, weak) IBOutlet UILabel *newsTitleLabel;

- (void) setTextViewText:(NSString *) text;

@end
