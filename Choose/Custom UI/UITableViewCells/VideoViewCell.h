//
//  LargeViewCell.h
//  Choose
//
//  Created by João Luís Mota on 2/9/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"

@interface VideoViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel * itemLabel;

@end
