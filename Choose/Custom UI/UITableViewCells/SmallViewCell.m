//
//  SmallViewCell.m
//  Choose
//
//  Created by João Luís Mota on 4/7/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "SmallViewCell.h"
#import "UIFontConstants.h"

@implementation SmallViewCell

#pragma mark -
#pragma mark - ViewLifecyle

- (void) awakeFromNib
{
    
    _itemLabel.font = kFONT_BEBAS_NEUE(18);
}

@end
