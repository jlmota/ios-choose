//
//  LargeViewCell.h
//  Choose
//
//  Created by João Luís Mota on 2/9/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"

@interface LargeViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView * itemImageView;
@property (nonatomic, weak) IBOutlet UILabel * itemLabel;

@end
