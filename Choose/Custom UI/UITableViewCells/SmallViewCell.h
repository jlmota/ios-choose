//
//  SmallViewCell.h
//  Choose
//
//  Created by João Luís Mota on 4/7/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"

@interface SmallViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView * frameImageView;
@property (nonatomic, weak) IBOutlet UIImageView * itemImageView;
@property (nonatomic, weak) IBOutlet UILabel * itemLabel;

@end
