//
//  CHTableViewCell.h
//  Choose
//
//  Created by João Luís Mota on 23/11/14.
//  Copyright (c) 2014 João Luís Mota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CHTableViewCell : UITableViewCell

@property (nonatomic, strong) NSString *cellType;

- (void)updateCell:(NSDictionary *)cellInfo;

@end
