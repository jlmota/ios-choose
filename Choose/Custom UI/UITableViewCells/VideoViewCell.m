//
//  LargeViewCell.m
//  Choose
//
//  Created by João Luís Mota on 2/9/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import "VideoViewCell.h"
#import "UIFontConstants.h"

@implementation VideoViewCell

#pragma mark -
#pragma mark - ViewLifecyle

- (void) awakeFromNib
{
    
    _itemLabel.font = kFONT_BEBAS(18);
}

@end
