//
//  CHTableViewCell.m
//  Choose
//
//  Created by João Luís Mota on 23/11/14.
//  Copyright (c) 2014 João Luís Mota. All rights reserved.
//

#import "CHTableViewCell.h"
#import "CHCatalogueMedia.h"
#import "UIFontConstants.h"
#import "UIImageView+AFNetworking.h"
#import "WSConfig.h"

#define kHEIGHT 168

@interface CHTableViewCell()

@property (nonatomic, strong) UIImageView *cellImageView;
@property (nonatomic, strong) UILabel *cellTitle;
@property (nonatomic, strong) UIWebView *videoWebView;
@property (nonatomic, strong) UIView *titleBackgroundView;


@end

@implementation CHTableViewCell


#pragma mark -
#pragma mark - ViewLifeCycle

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self initUI];
    }
    return self;
}

#pragma mark -
#pragma mark - PublicMethods

- (void)updateCell:(NSDictionary *)cellInfo
{
    if ([self.cellType isEqualToString:kCH_VIDEOS_METHOD]) {
        if (!self.videoWebView) {
            self.videoWebView = [[UIWebView alloc] initWithFrame:self.frame];
            self.videoWebView.backgroundColor = [UIColor blackColor];
            self.videoWebView.scrollView.scrollEnabled = NO;
            [self insertSubview:self.videoWebView aboveSubview:self.cellImageView];
            //[self sendSubviewToBack:self.videoWebView];
        }
        //clearcache
        //[self.videoWebView loadHTMLString:@"" baseURL:nil];
        
        NSString *html = @"<head><style type=\"text/css\">body, html { height: 100%; width: 100%; padding: 0; margin: 0; position: relative; min-height: 100%;}body div { width: 100%; height: 100%; margin: 0 auto; position: relative; }</style></head><body><div><iframe src=\"|VIDEO|\" width=\"100%\" height=\"100%\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div></body></html>";
        html = [html stringByReplacingOccurrencesOfString:@"|VIDEO|" withString:cellInfo[@"video"]];
        [self.videoWebView loadHTMLString:html baseURL:nil];
        
    }
    else {
        NSString *URL = cellInfo[@"image"];
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", kCH_URL, self.cellType, URL]];
        [self.cellImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"horizontal_default"]];
    }
    
    self.cellTitle.text = cellInfo[@"name"];
}

#pragma mark -
#pragma mark - PrivateMethods

- (void)initUI
{
    self.height = kHEIGHT;
    self.cellImageView = [[UIImageView alloc] initWithFrame:self.frame];
    self.cellImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.cellImageView.clipsToBounds = YES;
    
    self.titleBackgroundView = [[UIView alloc] init];
    self.titleBackgroundView.alpha = .6;
    self.titleBackgroundView.width = self.width;
    self.titleBackgroundView.height = 49;
    self.titleBackgroundView.backgroundColor = [UIColor blackColor];
    self.titleBackgroundView.y = self.height - self.titleBackgroundView.height;
    
    self.cellTitle = [[UILabel alloc] initWithFrame:self.titleBackgroundView.frame];
    self.cellTitle.font = kFONT_BEBAS_NEUE(20);
    self.cellTitle.textColor = [UIColor whiteColor];
    self.cellTitle.x = 20;
    self.cellTitle.width -=self.cellTitle.x;
    
    [self addSubview:self.cellImageView];
    [self addSubview:self.titleBackgroundView];
    [self addSubview:self.cellTitle];
}

@end
