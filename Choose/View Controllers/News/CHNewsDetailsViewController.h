//
//  NewsDetailsViewController.h
//  Choose
//
//  Created by João Luís Mota on 4/13/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHGernericViewController.h"
#import "CHNews.h"

@interface CHNewsDetailsViewController : CHGernericViewController

@property (nonatomic, strong) CHNews *news;

@end
