//
//  FirstViewController.m
//  Choose
//
//  Created by João Luís Mota on 4/3/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHNewsViewController.h"
#import "CHNewsDetailsViewController.h"
#import "DataController.h"
#import "LoggerManager.h"
#import "SegueConstants.h"
#import "IdentifierConstants.h"
#import "NIBConstants.h"
#import "DatabaseConstants.h"
#import "TableFetchedController.h"
#import "WSConfig.h"
#import "CHNews.h"
#import "HTProgressHUD.h"
#import "UserDefaultsManager.h"
#import "UserDefaultsConstants.h"
#import "CHTableViewCell.h"

#define kROW_HEIGHT 168

@interface CHNewsViewController ()<FetchedControllerProtocol, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *newsTableView;

@property (nonatomic, strong) TableFetchedController *newsTableFetchedController;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) HTProgressHUD *loadingView;


@end

@implementation CHNewsViewController

#pragma mark -
#pragma mark - ViewLifecyle
					
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self initUI];
    [self initTableFetchedController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self initLogger];
}

#pragma mark -
#pragma mark - PrivateMethod

- (void)initUI
{
    [super initUI];
    
    if (!self.newsTableView) {
        self.newsTableView = [[UITableView alloc] initWithFrame:self.view.frame];
        self.newsTableView.backgroundColor = [UIColor clearColor];
        self.newsTableView.separatorColor = [UIColor whiteColor];
        self.newsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.newsTableView.delegate = self;
        self.newsTableView.dataSource = self;
        self.newsTableView.height -=64;
        [self.newsTableView registerClass:[CHTableViewCell class] forCellReuseIdentifier:kTABLE_VIEW_CELL_IDENTIFIER];
        
        self.newsTableView.backgroundColor = [UIColor clearColor];
        
        self.refreshControl = [UIRefreshControl new];
        self.refreshControl.tintColor = [UIColor whiteColor];
        [self.refreshControl addTarget:self action:@selector(initLogger) forControlEvents:UIControlEventValueChanged];
        //[self.newsTableView addSubview:self.refreshControl];
        
        [self.view addSubview:self.newsTableView];
    }
}

- (void)initLogger
{
    if (kLOGGING) {
        [[LoggerManager sharedInstance] updateContent:NEWS_TYPE];
    } else {
        [[DataController sharedInstance] updateCHNews];
    }
}

- (void) endRefreshing
{
    [self.refreshControl endRefreshing];
}

- (void) showLoadingView
{
    if(!self.loadingView && ![UserDefaultsManager getObjectForKey:kCH_NEWS_TIME_STAMP]){
        self.loadingView = [[HTProgressHUD alloc] init];
        [self.loadingView showInView:self.view];
    }
}

- (void) hideLoadingView
{
    if(self.loadingView) {
        [self.loadingView hide];
    }
}

#pragma mark -
#pragma mark - FetchedController

- (void)initTableFetchedController
{
    self.newsTableFetchedController = [[TableFetchedController alloc] initWithTable:kCH_NEWS_TABLE
                                                                           predicate:nil
                                                                      andSortColumns:@[@"date"]
                                                                         isAscending:NO
                                                                            andLimit:5];
    
    self.newsTableFetchedController.delegate = self;
    
    DLog(@"[INFO]There are %ld CHNews.", (long)[self.newsTableFetchedController getNumberOfRows]);
}

#pragma mark -
#pragma mark - FetchedControllerProtocol

- (void) fetchedControllerDidChangeContent
{
    [self.newsTableView reloadData];
    [self endRefreshing];
    [self hideLoadingView];
}

#pragma mark -
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.newsTableFetchedController getNumberOfRows];
}

 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
     CHNews *news = (CHNews *)[self.newsTableFetchedController getManagedObjectAtIndex:indexPath.row];
     
     NSArray *keys = [[[news entity] attributesByName] allKeys];
     NSDictionary *dict = [news dictionaryWithValuesForKeys:keys];
     
     CHTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTABLE_VIEW_CELL_IDENTIFIER];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
     cell.cellType = kCH_NEWS_METHOD;
     [cell updateCell:dict];
 
     return cell;
 }

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kROW_HEIGHT;
}

#pragma mark -
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CHNews *news = (CHNews *)[self.newsTableFetchedController getManagedObjectAtIndex:indexPath.row];
    
    [kCHAppDelegate sendEvent:kGA_NEWS_CATEGORY forAction:kGA_DETAIL_TAPPED_EVENT withLabel:news.name andValue:nil];
    
    CHNewsDetailsViewController *newsDetailsViewController = [[CHNewsDetailsViewController alloc] init];
    newsDetailsViewController.news = news;
    [self.navigationController pushViewController:newsDetailsViewController animated:YES];
}

#pragma mark -
#pragma mark - GA

- (NSString *)screenName
{
    return kGA_SCREEN_NAME_NEWS;
}

@end
