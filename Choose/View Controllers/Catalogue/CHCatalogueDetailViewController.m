//
//  CHCatalogueDetailViewController.m
//  Choose
//
//  Created by João Luís Mota on 2/9/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import "CHCatalogueDetailViewController.h"
#import "CHQueryHelper.h"
#import "UIImageView+AFNetworking.h"
#import "WSConfig.h"
#import "TableFetchedController.h"
#import "CHCatalogueMedia.h"
#import "IdentifierConstants.h"
#import "DataController.h"
#import "DatabaseConstants.h"
#import "ConnectivitySingleton.h"

@interface CHCatalogueDetailViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, FetchedControllerProtocol>

@property (nonatomic, strong) UICollectionView *catalogueCollectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *collectionViewFlowLayout;
@property (nonatomic, strong) TableFetchedController *catalogueTableFetchedController;
@property (nonatomic) BOOL gridLayout;

@end

@implementation CHCatalogueDetailViewController

#pragma mark -
#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.gridLayout = YES;
    self.child = YES;
    
    [self initTableFetchedController];
    [self getCatalogueMediaFromServer];
    [self initUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - PrivateMethods

- (void)initUI
{
    [super initUI];
    
    if (!self.catalogueCollectionView) {
        self.collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        
        self.catalogueCollectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:self.collectionViewFlowLayout];
        [self.catalogueCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:kCATALOGUE_COLLECTION_CELL_IDENTIFIER];
        self.catalogueCollectionView.backgroundColor = [UIColor clearColor];
        self.catalogueCollectionView.delegate = self;
        self.catalogueCollectionView.dataSource = self;
        self.catalogueCollectionView.height -= 64;
        self.catalogueCollectionView.centerX = self.view.centerX;
        self.catalogueCollectionView.pagingEnabled = YES;
        self.catalogueCollectionView.showsHorizontalScrollIndicator = NO;
        self.catalogueCollectionView.showsVerticalScrollIndicator = NO;
        
        self.collectionViewFlowLayout.itemSize = self.catalogueCollectionView.frame.size;
        self.collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        [self.view addSubview:self.catalogueCollectionView];
    }
    
}

- (void)getCatalogueMediaFromServer
{
    if ([[ConnectivitySingleton sharedInstance] isOnline]) {
        [[DataController sharedInstance] updateCHCatalogueMedia:self.currentCatalogue.tableID];
    }
}

#pragma mark -
#pragma mark - FetchedController

- (void)initTableFetchedController
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"catID = %@", self.currentCatalogue.tableID];
    
    self.catalogueTableFetchedController = [[TableFetchedController alloc] initWithTable:kCH_CATALOGUE_MEDIA_TABLE
                                                                               predicate:predicate
                                                                          andSortColumns:@[@"date"]
                                                                             isAscending:NO
                                                                                andLimit:0];
    
    self.catalogueTableFetchedController.delegate = self;
    
    DLog(@"[INFO]There are %ld CHNews.", (long)[self.catalogueTableFetchedController getNumberOfRows]);
}

#pragma mark -
#pragma mark - FetchedControllerProtocol

- (void) fetchedControllerDidChangeContent
{
    [self.catalogueCollectionView reloadData];
}

#pragma mark -
#pragma mark - UICollectionViewDelegate

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return !self.gridLayout ? 2 : 0;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return !self.gridLayout ? 2 : 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.catalogueTableFetchedController getNumberOfRows];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.gridLayout) {
        self.catalogueCollectionView.pagingEnabled = YES;
        [self.collectionViewFlowLayout setItemSize:CGSizeMake(self.catalogueCollectionView.width, self.catalogueCollectionView.height)];
        [self.collectionViewFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        self.gridLayout = YES;
    }
    else {
        [self.collectionViewFlowLayout setItemSize:CGSizeMake(105, 105)];
        [self.collectionViewFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        self.catalogueCollectionView.pagingEnabled = NO;
        self.gridLayout = NO;
    }
    
    [self.catalogueCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    
    self.catalogueCollectionView.collectionViewLayout = self.collectionViewFlowLayout;
    [self.catalogueCollectionView reloadData];
}

#pragma mark -
#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CHCatalogueMedia *media = (CHCatalogueMedia *)[self.catalogueTableFetchedController getManagedObjectAtIndex:indexPath.row];
    NSString *URL = media.image;
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", kCH_URL, kCH_CATALOGUE_MEDIA_METHOD, URL]];

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCATALOGUE_COLLECTION_CELL_IDENTIFIER forIndexPath:indexPath];
    
    UIImageView *catalogueImageView = [[UIImageView alloc] init];
    [catalogueImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"vertical_default"]];
    catalogueImageView.width = cell.width;
    catalogueImageView.height = cell.height;
    catalogueImageView.contentMode = UIViewContentModeScaleAspectFill;
    catalogueImageView.clipsToBounds = YES;

    cell.backgroundView = catalogueImageView;
    
    return cell;
}

#pragma mark -
#pragma mark - GA

- (NSString *)screenName
{
    return kGA_SCREEN_NAME_COLLECTION_DETAILS;
}

@end
