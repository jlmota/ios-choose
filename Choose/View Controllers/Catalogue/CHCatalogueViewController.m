//
//  SecondViewController.m
//  Choose
//
//  Created by João Luís Mota on 4/3/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHCatalogueViewController.h"
#import "CHCatalogueDetailViewController.h"
#import "IdentifierConstants.h"
#import "NIBConstants.h"
#import "SegueConstants.h"
#import "DataController.h"
#import "LoggerManager.h"
#import "WSConfig.h"
#import "TableFetchedController.h"
#import "DatabaseConstants.h"
#import "CHCatalogue.h"
#import "CHCatalogueMedia.h"
#import "CHTableViewCell.h"

#define kROW_HEIGHT 168

@interface CHCatalogueViewController ()<FetchedControllerProtocol, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *catalogueTableView;
@property (nonatomic, strong) TableFetchedController *catalogueTableFetchedController;

@end

@implementation CHCatalogueViewController

@synthesize catalogueTableView;
@synthesize catalogueTableFetchedController;

#pragma mark -
#pragma mark - ViewLifecyle
				
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self initTableFetchedController];
    [self initUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self initLogger];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - PrivateMethod

- (void)initUI
{
    if (!self.catalogueTableView) {
        [super initUI];
        
        self.catalogueTableView = [[UITableView alloc] initWithFrame:self.view.frame];
        self.catalogueTableView.backgroundColor = [UIColor clearColor];
        self.catalogueTableView.separatorColor = [UIColor whiteColor];
        self.catalogueTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.catalogueTableView.delegate = self;
        self.catalogueTableView.dataSource = self;
        self.catalogueTableView.height -=64;
        [self.catalogueTableView registerClass:[CHTableViewCell class] forCellReuseIdentifier:kTABLE_VIEW_CELL_IDENTIFIER];
        
        [self.view addSubview:self.catalogueTableView];
    }
}

- (void)initLogger
{
    if (kLOGGING) {
        [[LoggerManager sharedInstance] updateContent:CATALOGUE_TYPE];
    } else {
        [[DataController sharedInstance] updateCHCatalogue];
    }
}

#pragma mark -
#pragma mark - FetchedController

- (void)initTableFetchedController
{
    self.catalogueTableFetchedController = [[TableFetchedController alloc] initWithTable:kCH_CATALOGUE_TABLE
                                                                               predicate:nil
                                                                          andSortColumns:@[@"date"]
                                                                             isAscending:NO
                                                                                andLimit:0];
    
    self.catalogueTableFetchedController.delegate = self;
    
    DLog(@"[INFO]There are %ld CHNews.", (long)[self.catalogueTableFetchedController getNumberOfRows]);
}

#pragma mark -
#pragma mark - FetchedControllerProtocol

- (void) fetchedControllerDidChangeContent
{
    [self.catalogueTableView reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.catalogueTableFetchedController getNumberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CHCatalogue *catalogue = (CHCatalogue *)[self.catalogueTableFetchedController getManagedObjectAtIndex:indexPath.row];
    
    NSArray *keys = [[[catalogue entity] attributesByName] allKeys];
    NSDictionary *dict = [catalogue dictionaryWithValuesForKeys:keys];
    
    CHTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTABLE_VIEW_CELL_IDENTIFIER];
    cell.cellType = kCH_CATALOGUES_METHOD;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCell:dict];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kROW_HEIGHT;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CHCatalogue *catalogue = (CHCatalogue *)[self.catalogueTableFetchedController getManagedObjectAtIndex:indexPath.row];
    
    [kCHAppDelegate sendEvent:kGA_COLLECTION_CATEGORY forAction:kGA_DETAIL_TAPPED_EVENT withLabel:catalogue.name andValue:nil];
    
    CHCatalogueDetailViewController *catalogueDetailController = [[CHCatalogueDetailViewController alloc] init];
    catalogueDetailController.currentCatalogue = catalogue;
    [self.navigationController pushViewController:catalogueDetailController animated:YES];
    
}

#pragma mark -
#pragma mark - GA

- (NSString *)screenName
{
    return kGA_SCREEN_NAME_COLLECTION;
}

@end
