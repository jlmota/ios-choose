//
//  CHCatalogueDetailViewController.h
//  Choose
//
//  Created by João Luís Mota on 2/9/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHGernericViewController.h"
#import "CHCatalogue.h"

@interface CHCatalogueDetailViewController : CHGernericViewController

@property (nonatomic, strong) CHCatalogue *currentCatalogue;

@end
