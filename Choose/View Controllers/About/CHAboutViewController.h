//
//  SecondViewController.h
//  Choose
//
//  Created by João Luís Mota on 4/3/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHGernericViewController.h"

@interface CHAboutViewController : CHGernericViewController

@end
