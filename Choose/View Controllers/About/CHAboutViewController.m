//
//  SecondViewController.m
//  Choose
//
//  Created by João Luís Mota on 4/3/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHAboutViewController.h"
#import "AppConstant.h"

@interface CHAboutViewController()

@property (nonatomic, strong) UILabel *aboutLabel;

@end

@implementation CHAboutViewController
							
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self initUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initUI {
    [super initUI];
    
    if (!self.aboutLabel) {
        UIScrollView *containerScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
        containerScrollView.height -= 64;
        
        self.aboutLabel = [[UILabel alloc] initWithFrame:self.view.frame];
        self.aboutLabel.backgroundColor = [UIColor clearColor];
        self.aboutLabel.y = 0;
        self.aboutLabel.width -= 30;
        self.aboutLabel.centerX = self.view.centerX;
        self.aboutLabel.font = kFONT_BEBAS_NEUE(16);
        self.aboutLabel.numberOfLines = 0;
        self.aboutLabel.textColor = [UIColor whiteColor];
        
        NSArray *relevanteWordsArr = @[@"streetwear portuguesa", @"pormenor e a preocupação em criar", @"escolhas", @"inspirando-se no mundo", @"colecções temáticas", @"criar ligações", @"desportos de acção", @"capacidades dos jovens", @"valorizamos"];
        
        [self buildAttributedText:relevanteWordsArr];
        
        [self.aboutLabel sizeToFit];
        
        UIButton *instagramButton = [[UIButton alloc] init];
        instagramButton.width = 220;
        instagramButton.height = 50;
        instagramButton.y = self.aboutLabel.y + self.aboutLabel.height + 10;
        instagramButton.centerX = containerScrollView.centerX;
        instagramButton.titleLabel.font = kFONT_BEBAS_NEUE(20);
        instagramButton.titleLabel.textColor = [UIColor whiteColor];
        instagramButton.layer.borderWidth = 2;
        instagramButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [instagramButton setTitle:@"INSTAGRAM" forState:UIControlStateNormal];
        [instagramButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [instagramButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [instagramButton addTarget:self action:@selector(buttonsTarget:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *websiteButton = [[UIButton alloc] init];
        websiteButton.width = 220;
        websiteButton.height = 50;
        websiteButton.y = instagramButton.y + instagramButton.height + 10;
        websiteButton.centerX = containerScrollView.centerX;
        websiteButton.titleLabel.font = kFONT_BEBAS_NEUE(20);
        websiteButton.titleLabel.textColor = [UIColor whiteColor];
        websiteButton.layer.borderWidth = 2;
        websiteButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [websiteButton setTitle:@"WWW.CHOOSE-CLHOTHING.COM" forState:UIControlStateNormal];
        [websiteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [websiteButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [websiteButton addTarget:self action:@selector(buttonsTarget:) forControlEvents:UIControlEventTouchUpInside];
        
        [containerScrollView addSubview:self.aboutLabel];
        [containerScrollView addSubview:instagramButton];
        [containerScrollView addSubview:websiteButton];
        
        containerScrollView.contentSizeHeight = websiteButton.y + websiteButton.height + 40;
        
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"about_background"]];
        
        [self.view addSubview:backgroundImageView];
        [self.view addSubview:containerScrollView];

    }
}

- (void)buildAttributedText:(NSArray *)relevantWord
{
    NSString *allText = kABOUT_TEXT;
    
    UIFont *normalBebasFont = kFONT_BEBAS_NEUE(17);
    UIFont *bigBebasFont = kFONT_BEBAS_NEUE(25);
    
    NSDictionary *norlmalBebasDic = [NSDictionary dictionaryWithObject:normalBebasFont forKey:NSFontAttributeName];
    NSDictionary *bigBebasDic = [NSDictionary dictionaryWithObject:bigBebasFont forKey:NSFontAttributeName];
    
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:kABOUT_TEXT attributes:norlmalBebasDic];
    
    for (int i = 0; i != relevantWord.count; i++) {
        NSRange range = [allText rangeOfString:relevantWord[i]];
        [aAttrString setAttributes:bigBebasDic range:range];
    }
    
    self.aboutLabel.attributedText = aAttrString;
}

- (IBAction)buttonsTarget:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"INSTAGRAM"]) {
        NSURL *instagramURL = [NSURL URLWithString:@"instagram://user?username=chooseclothing"];
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
            [[UIApplication sharedApplication] openURL:instagramURL];
        }
        else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://instagram.com/chooseclothing/"]];
        }
        
        [kCHAppDelegate sendEvent:kGA_ABOUT_CATEGORY forAction:kGA_BUTTON_TAPPED_EVENT withLabel:@"INSTAGRAM" andValue:nil];
    }
    else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.choose-clothing.com/"]];
        
        [kCHAppDelegate sendEvent:kGA_ABOUT_CATEGORY forAction:kGA_BUTTON_TAPPED_EVENT withLabel:@"CHOOSE-WEBSITE" andValue:nil];
    }
}

#pragma mark -
#pragma mark - GA

- (NSString *)screenName
{
    return kGA_SCREEN_NAME_ABOUT;
}

@end
