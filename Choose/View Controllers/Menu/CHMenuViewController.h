//
//  MenuViewController.h
//  Choose
//
//  Created by João Luís Mota on 08/11/14.
//  Copyright (c) 2014 João Luís Mota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHGernericViewController.h"

@interface CHMenuViewController : CHGernericViewController

@end
