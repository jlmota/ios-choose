//
//  MenuViewController.m
//  Choose
//
//  Created by João Luís Mota on 08/11/14.
//  Copyright (c) 2014 João Luís Mota. All rights reserved.
//

#import "CHMenuViewController.h"
#import "PListConstants.h"
#import "PListManager.h"
#import "UIConstants.h"
#import "IdentifierConstants.h"
#import "UIFontConstants.h"
#import "UIColorCosntants.h"

@interface CHMenuViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *menuTableView;
@property (nonatomic, strong) UITableViewCell *selectedViewCell;
@property (nonatomic, strong) NSArray *menuItemArray;

@end

@implementation CHMenuViewController

#pragma mark -
#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initMenuArray];
    [self initUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - PrivateMethods

- (void)initUI
{
    if (!self.menuTableView) {
        self.view.backgroundColor = [UIColor blackColor];
        
        self.menuTableView = [[UITableView alloc] initWithFrame:self.view.frame];
        self.menuTableView.y = 20;
        self.menuTableView.height -=20;
        self.menuTableView.backgroundColor = [UIColor clearColor];
        self.menuTableView.separatorColor = [UIColor whiteColor];
        self.menuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.menuTableView.delegate = self;
        self.menuTableView.dataSource = self;
        [self.menuTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kMENU_CELL_IDENTIFIER];
        
        [self.view addSubview:self.menuTableView];
    }
}

- (void)initMenuArray
{
    self.menuItemArray = [PListManager getArrayPListWithName:kMENU_LIST];
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 64;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * menuIonfoDic = [self.menuItemArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [UIColor whiteColor];
    
    if (self.selectedViewCell) {
        self.selectedViewCell.textLabel.textColor = [UIColor whiteColor];
        self.selectedViewCell.backgroundColor = [UIColor clearColor];
    }
    
    UIViewController *nextViewController = [[NSClassFromString(menuIonfoDic[@"viewController"]) alloc] init];
    UINavigationController *centerNavigationViewController = [[UINavigationController alloc] initWithRootViewController:nextViewController];
    
    [self.mm_drawerController setCenterViewController:centerNavigationViewController
                               withFullCloseAnimation:YES
                                           completion:nil];
    
    self.selectedViewCell = cell;
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tableHeaderView = [[UIView alloc] init];
    tableHeaderView.width = tableView.width;
    tableHeaderView.height = 64;
    tableHeaderView.backgroundColor = [UIColor clearColor];
    
    return tableHeaderView;
}


#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuItemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * menuIonfoDic = [self.menuItemArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kMENU_CELL_IDENTIFIER];
    cell.textLabel.font = kFONT_BEBAS(19);
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = menuIonfoDic[@"name"];
    cell.backgroundColor = [UIColor clearColor];
    cell.indentationLevel = 1;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (![tableView indexPathForSelectedRow]) {
        [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                               animated:NO
                         scrollPosition:UITableViewScrollPositionTop];
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor whiteColor];
        
        self.selectedViewCell = cell;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - GA

- (NSString *)screenName
{
    return kGA_SCREEN_NAME_MENU;
}


@end
