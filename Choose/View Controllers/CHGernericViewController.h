//
//  GernericViewController.h
//  Choose
//
//  Created by João Luís Mota on 5/19/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDevice+Utils.h"
#import "UIViewController+MMDrawerController.h"
#import "UIFontConstants.h"
#import "UIColorCosntants.h"
#import "GAConstants.h"
#import "CHAppDelegate.h"

@interface CHGernericViewController : GAITrackedViewController

@property (nonatomic) BOOL child;
@property float contetOffset;

- (void)initUI;

@end
