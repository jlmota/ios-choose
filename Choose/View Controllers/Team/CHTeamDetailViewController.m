//
//  TeamDetailViewController.m
//  Choose
//
//  Created by João Luís Mota on 6/22/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import "CHTeamDetailViewController.h"
#import "AppConstant.h"
#import "CHNews.h"
#import "NIBConstants.h"
#import "TeamHeaderView.h"
#import "TeamContentView.h"
#import "UIImageView+AFNetworking.h"
#import "WSConfig.h"
#import "UIColorCosntants.h"
#import "UIFontConstants.h"

#define kHEADER_HEIGHT 187
#define kHEADER_REAL_HEIGHT 318

@interface CHTeamDetailViewController()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *newssScrollView;
@property (nonatomic, strong) UIImageView *defaultImageView;

@end

@implementation CHTeamDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.child = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self initUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - PrivateMethods

- (void)initUI
{
    [super initUI];
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (!self.newssScrollView) {
        NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@", kCH_URL, kCH_TEAM_METHOD, self.teamMember.image]];
        
        self.defaultImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"horizontal_default"]];;
        [self.defaultImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"horizontal_default"]];
        self.defaultImageView.width = self.view.width;
        self.defaultImageView.height = 174;
        self.defaultImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.defaultImageView.clipsToBounds = YES;
        
        UIView *bodyView = [self buildBodyView];
        bodyView.y = self.defaultImageView.height;
        
        CGFloat conteSizeHeight = bodyView.height + self.defaultImageView.height + 64;
        
        self.newssScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
        self.newssScrollView.contentSizeHeight = self.view.height < conteSizeHeight ? conteSizeHeight : self.view.height + 1;
        self.newssScrollView.delegate = self;
        self.newssScrollView.backgroundColor = [UIColor clearColor];
        self.newssScrollView.showsVerticalScrollIndicator = NO;
        [self.newssScrollView addSubview:bodyView];
        
        [self.view addSubview:self.defaultImageView];
        [self.view addSubview:self.newssScrollView];
    }
}

- (UIView *)buildBodyView
{
    UILabel *newsTitleLabel = [[UILabel alloc] init];
    newsTitleLabel.x = 20;
    newsTitleLabel.y = 16;
    newsTitleLabel.font = kFONT_BEBAS_NEUE(20);
    newsTitleLabel.text = self.teamMember.name;
    [newsTitleLabel sizeToFit];
    
    UITextView *newsTextView = [[UITextView alloc] init];
    newsTextView.x = newsTitleLabel.x - 5;
    newsTextView.y = newsTitleLabel.y + newsTitleLabel.height + 10;
    newsTextView.font = kFONT_BEBAS_NEUE(15);
    newsTextView.text = self.teamMember.text;
    newsTextView.width = self.view.width - newsTextView.x * 2;
    newsTextView.dataDetectorTypes = UIDataDetectorTypeAll;
    newsTextView.editable = NO;
    newsTextView.tintColor = [UIColor darkGrayColor];
    [newsTextView sizeToFit];

    
    UIView *bodyContainer = [[UIView alloc] init];
    bodyContainer.width = self.view.width;
    bodyContainer.height = newsTextView.y + newsTextView.height + 10;
    bodyContainer.backgroundColor = [UIColor whiteColor];
    
    [bodyContainer addSubview:newsTitleLabel];
    [bodyContainer addSubview:newsTextView];
    
    return bodyContainer;
}

#pragma mark -
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat scale = (scrollView.contentOffsetY * -1)/100 + 1;
    
    if (scale > 1) {
        self.defaultImageView.transform = CGAffineTransformMakeScale(scale, scale);
    }
    else {
        self.defaultImageView.y = (-scrollView.contentOffsetY) *.4;
    }
}

#pragma mark -
#pragma mark - GA

- (NSString *)screenName
{
    return kGA_SCREEN_NAME_TEAM_RIDERS_DETAILS;
}

@end
