//
//  SecondViewController.m
//  Choose
//
//  Created by João Luís Mota on 4/3/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHTeamViewController.h"
#import "CHTeamDetailViewController.h"
#import "IdentifierConstants.h"
#import "NIBConstants.h"
#import "SegueConstants.h"
#import "DataController.h"
#import "LoggerManager.h"
#import "WSConfig.h"
#import "TableFetchedController.h"
#import "DatabaseConstants.h"
#import "CHTeam.h"
#import "CHTableViewCell.h"

#define kROW_HEIGHT 168

@interface CHTeamViewController ()<FetchedControllerProtocol>

@property (nonatomic, strong) UITableView *teamTableView;

@property (nonatomic, strong) TableFetchedController *teamTableFetchedController;

@end

@implementation CHTeamViewController

@synthesize teamTableView;
@synthesize teamTableFetchedController;

#pragma mark -
#pragma mark - ViewLifecyle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self initTableFetchedController];
    [self initUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self initLogger];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - PrivateMethod

- (void)initUI
{
    if (!self.teamTableView) {
        [super initUI];
        
        self.teamTableView = [[UITableView alloc] initWithFrame:self.view.frame];
        self.teamTableView.backgroundColor = [UIColor clearColor];
        self.teamTableView.separatorColor = [UIColor whiteColor];
        self.teamTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.teamTableView.delegate = self;
        self.teamTableView.dataSource = self;
        self.teamTableView.height -=64;
        [self.teamTableView registerClass:[CHTableViewCell class] forCellReuseIdentifier:kTABLE_VIEW_CELL_IDENTIFIER];
        
        [self.view addSubview:self.teamTableView];
    }
}

- (void)initLogger
{
    if (kLOGGING) {
        [[LoggerManager sharedInstance] updateContent:TEAM_TYPE];
    } else {
        [[DataController sharedInstance] updateCHTeam];
    }
}

#pragma mark -
#pragma mark - FetchedController

- (void)initTableFetchedController
{
    self.teamTableFetchedController = [[TableFetchedController alloc] initWithTable:kCH_TEAM_TABLE
                                                                               predicate:nil
                                                                          andSortColumns:@[@"date"]
                                                                             isAscending:NO
                                                                                andLimit:0];
    
    self.teamTableFetchedController.delegate = self;
    
    DLog(@"[INFO]There are %ld CHTeam.", (long)[self.teamTableFetchedController getNumberOfRows]);
}

#pragma mark -
#pragma mark - FetchedControllerProtocol

- (void) fetchedControllerDidChangeContent
{
    [self.teamTableView reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.teamTableFetchedController getNumberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CHTeam *team = (CHTeam *)[self.teamTableFetchedController getManagedObjectAtIndex:indexPath.row];
    
    NSArray *keys = [[[team entity] attributesByName] allKeys];
    NSDictionary *dict = [team dictionaryWithValuesForKeys:keys];
    
    CHTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTABLE_VIEW_CELL_IDENTIFIER];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.cellType = kCH_TEAM_METHOD;
    [cell updateCell:dict];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return kROW_HEIGHT;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CHTeam *team = (CHTeam *)[self.teamTableFetchedController getManagedObjectAtIndex:indexPath.row];
    
    [kCHAppDelegate sendEvent:kGA_TEAM_CATEGORY forAction:kGA_DETAIL_TAPPED_EVENT withLabel:team.name andValue:nil];
    
    CHTeamDetailViewController *teamDetailController = [[CHTeamDetailViewController alloc] init];
    teamDetailController.teamMember = team;
    [self.navigationController pushViewController:teamDetailController animated:YES];
}

#pragma mark -
#pragma mark - GA

- (NSString *)screenName
{
    return kGA_SCREEN_NAME_TEAM_RIDERS;
}

@end
