//
//  TeamDetailViewController.h
//  Choose
//
//  Created by João Luís Mota on 6/22/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHGernericViewController.h"
#import "CHTeam.h"

@interface CHTeamDetailViewController : CHGernericViewController

@property (nonatomic, strong) CHTeam *teamMember;

@end
