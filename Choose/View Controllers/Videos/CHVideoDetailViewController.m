//
//  CHVideoDetailViewController.m
//  Choose
//
//  Created by João Luís Mota on 4/6/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import "CHVideoDetailViewController.h"

@interface CHVideoDetailViewController ()

@property (nonatomic, weak) IBOutlet UIWebView *videoWebView;
@property (nonatomic, weak) IBOutlet UIView *loadingView;

@end

@implementation CHVideoDetailViewController

@synthesize videoWebView;
@synthesize loadingView;
@synthesize webViewURLString;

#pragma mark -
#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.videoWebView.delegate = self;
    [self.videoWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.webViewURLString]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 
#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.loadingView.hidden = YES;
}

@end
