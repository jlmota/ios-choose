//
//  CHVideoDetailViewController.h
//  Choose
//
//  Created by João Luís Mota on 4/6/14.
//  Copyright (c) 2014 Choose. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CHGernericViewController.h"

@interface CHVideoDetailViewController : CHGernericViewController<UIWebViewDelegate>

@property (nonatomic, strong) NSString *webViewURLString;

@end
