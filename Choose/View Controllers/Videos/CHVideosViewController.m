//
//  SecondViewController.m
//  Choose
//
//  Created by João Luís Mota on 4/3/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#import "CHVideosViewController.h"
#import "CHVideoDetailViewController.h"
#import "CHTableViewCell.h"
#import "IdentifierConstants.h"
#import "NIBConstants.h"
#import "SegueConstants.h"
#import "DataController.h"
#import "LoggerManager.h"
#import "WSConfig.h"
#import "TableFetchedController.h"
#import "DatabaseConstants.h"
#import "CHVideo.h"

#define kROW_HEIGHT 168

@interface CHVideosViewController ()<FetchedControllerProtocol>

@property (nonatomic, strong) UITableView *videoTableView;

@property (nonatomic, strong) TableFetchedController *videoTableFetchedController;

@end

@implementation CHVideosViewController

@synthesize videoTableView;
@synthesize videoTableFetchedController;

#pragma mark -
#pragma mark - ViewLifecyle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self initTableFetchedController];
    [self initUI];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self initLogger];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kVIDEO_DETAIL_SEGUE]) {
        NSIndexPath *indexPath = [self.videoTableView indexPathForSelectedRow];
        
        CHVideo *videoDetail = (CHVideo *)[self.videoTableFetchedController getManagedObjectAtIndex:indexPath.row];
        
        CHVideoDetailViewController *videoDetails = segue.destinationViewController;
        videoDetails.webViewURLString = videoDetail.video;
    }
}

#pragma mark -
#pragma mark - PrivateMethod

- (void)initUI
{
    [super initUI];
    if (!self.videoTableView) {
        [super initUI];
        
        self.videoTableView = [[UITableView alloc] initWithFrame:self.view.frame];
        self.videoTableView.backgroundColor = [UIColor clearColor];
        self.videoTableView.separatorColor = [UIColor whiteColor];
        self.videoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.videoTableView.delegate = self;
        self.videoTableView.dataSource = self;
        self.videoTableView.height -=64;
        [self.videoTableView registerClass:[CHTableViewCell class] forCellReuseIdentifier:kTABLE_VIEW_CELL_IDENTIFIER];
        
        [self.view addSubview:self.videoTableView];
    }
}

- (void)initLogger
{
    if (kLOGGING) {
        [[LoggerManager sharedInstance] updateContent:VIDEO_TYPE];
    } else {
        [[DataController sharedInstance] updateCHVideos];
    }
}

#pragma mark -
#pragma mark - FetchedController

- (void)initTableFetchedController
{
    self.videoTableFetchedController = [[TableFetchedController alloc] initWithTable:kCH_VIDEO_TABLE
                                                                           predicate:nil
                                                                      andSortColumns:@[@"date"]
                                                                         isAscending:NO
                                                                            andLimit:0];
    
    self.videoTableFetchedController.delegate = self;
    
    DLog(@"[INFO]There are %ld CHVideo.", (long)[self.videoTableFetchedController getNumberOfRows]);
}

#pragma mark -
#pragma mark - FetchedControllerProtocol

- (void) fetchedControllerDidChangeContent
{
    [self.videoTableView reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.videoTableFetchedController getNumberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CHVideo *video = (CHVideo *)[self.videoTableFetchedController getManagedObjectAtIndex:indexPath.row];
    
    NSArray *keys = [[[video entity] attributesByName] allKeys];
    NSDictionary *dict = [video dictionaryWithValuesForKeys:keys];
    
    CHTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTABLE_VIEW_CELL_IDENTIFIER];
    cell.cellType = kCH_VIDEOS_METHOD;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCell:dict];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kROW_HEIGHT;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark -
#pragma mark - GA

- (NSString *)screenName
{
    return kGA_SCREEN_NAME_VIDEOS;
}

@end
