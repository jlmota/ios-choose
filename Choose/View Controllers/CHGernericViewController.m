//
//  GernericViewController.m
//  Choose
//
//  Created by João Luís Mota on 5/19/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#import "CHGernericViewController.h"
#import "AppConstant.h"
#import "UIDevice+Utils.h"
#import "UIViewController+MMDrawerController.h"
#import "UIFontConstants.h"
#import "CHAboutViewController.h"

@interface CHGernericViewController ()

@end

@implementation CHGernericViewController

#pragma mark -
#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self initUI];
}

#pragma mark -
#pragma mark - PrivateMethods

- (void)initUI
{
    self.view.backgroundColor = [self isKindOfClass:[CHAboutViewController class]] ? kGREY_COLOR_TWO : [UIColor blackColor];
    _contetOffset = 0;
    
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header_logo"]];
    self.navigationItem.leftBarButtonItem = [self leftBarButtonItem];
}

- (UIBarButtonItem *)leftBarButtonItem
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (self.child) {
        button.titleLabel.font = kFONT_BEBAS_NEUE(20);
        [button setTitle:@"BACK" forState:UIControlStateNormal];
    }
    else {
        [button setImage:[UIImage imageNamed:@"header_icon_menu"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"header_icon_menu"] forState:UIControlStateHighlighted];
    }
    
    [button addTarget:self action:@selector(leftMenuBarButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    
    return [[UIBarButtonItem alloc] initWithCustomView: button];
}

#pragma mark -
#pragma mark - IBAction

-(IBAction)leftMenuBarButtonPressed:(id)sender{
    if (self.child) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    }
}

@end
