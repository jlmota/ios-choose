//
//  CHCatalogueMedia.h
//  Choose
//
//  Created by João Luís Mota on 17/07/15.
//  Copyright (c) 2015 João Luís Mota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CHCatalogueMedia : NSManagedObject

@property (nonatomic, retain) NSNumber * catID;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSNumber * tableID;

@end
