//
//  CHTeam.m
//  Choose
//
//  Created by João Luís Mota on 17/07/15.
//  Copyright (c) 2015 João Luís Mota. All rights reserved.
//

#import "CHTeam.h"


@implementation CHTeam

@dynamic date;
@dynamic image;
@dynamic name;
@dynamic tableID;
@dynamic text;

@end
