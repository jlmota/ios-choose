//
//  CHCatalogueMedia.m
//  Choose
//
//  Created by João Luís Mota on 17/07/15.
//  Copyright (c) 2015 João Luís Mota. All rights reserved.
//

#import "CHCatalogueMedia.h"


@implementation CHCatalogueMedia

@dynamic catID;
@dynamic date;
@dynamic image;
@dynamic tableID;

@end
