//
//  CHVideo.h
//  Choose
//
//  Created by João Luís Mota on 17/07/15.
//  Copyright (c) 2015 João Luís Mota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CHVideo : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * tableID;
@property (nonatomic, retain) NSString * video;

@end
