//
//  SegueConstants.h
//  Choose
//
//  Created by João Luís Mota on 10/11/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#define kSMALL_CELL_IDENTIFIER @"SmallCellIdentifier"
#define kLARGE_CELL_IDENTIFIER @"LargeCellIdentifier"
#define kVIDEO_CELL_IDENTIFIER @"VideoCellIdentifier"
#define kMENU_CELL_IDENTIFIER @"MenuCellIdentifier"
#define kCATALOGUE_COLLECTION_CELL_IDENTIFIER @"CatalogueCollectionCell"
#define kTABLE_VIEW_CELL_IDENTIFIER @"TableViewCell"

