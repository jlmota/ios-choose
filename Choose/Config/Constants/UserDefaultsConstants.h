//
//  UserDefaultsConstants.h
//  Choose
//
//  Created by João Luís Mota on 10/11/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//
//Loggers TimeStamp
#define kCH_NEWS_TIME_STAMP @"CHNewsTimeStamp"
#define kCH_VIDEO_TIME_STAMP @"CHVideoTimeStamp"
#define kCH_TEAM_TIME_STAMP @"CHTeamTimeStamp"
#define kCH_CATALOGUE_TIME_STAMP @"CHCatalogueTimeStamp"
#define kCH_CATALOGUE_MEDIA_TIME_STAMP @"CHCatalogueMediaTimeStamp"

//AutoIncrement ID's
#define kCH_CATALOGUE_IMAGE_ID @"CHCatalogueImageID"
#define kCH_CATALOGUE_VIDEO_ID @"CHCatalogueVideoID"

