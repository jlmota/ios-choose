//
//  SegueConstants.h
//  Choose
//
//  Created by João Luís Mota on 10/11/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#define kNEWS_DETAIL_SEGUE @"NewsDetailSegue"
#define kCATALOGUE_DETAIL_SEGUE @"CatalogueDetailSegue"
#define kVIDEO_DETAIL_SEGUE @"VideoDetailSegue"

