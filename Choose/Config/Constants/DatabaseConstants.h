//
//  DatabaseConstants.h
//  Choose
//
//  Created by João Luís Mota on 10/11/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#define kIS_LOGGING 1

#define kCH_DATABASE_MODEL @"CHModel"

#define kCH_NEWS_TABLE @"CHNews"
#define kCH_VIDEO_TABLE @"CHVideo"
#define kCH_TEAM_TABLE @"CHTeam"
#define kCH_CATALOGUE_TABLE @"CHCatalogue"
#define kCH_CATALOGUE_MEDIA_TABLE @"CHCatalogueMedia"

#define kTABLE_ID_ATTR @"tableID"

#define kNULL  @"null"