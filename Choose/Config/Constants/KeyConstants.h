//
//  KeyConstants.h
//  Choose
//
//  Created by João Luís Mota on 10/11/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

//Database keys
#define kDB_VERSION_KEY @"db_version"
#define kC_INFO_TIMESTAMP @"c_info_timestamp"

//TabBar
#define kTAB_ICON_IMAGE_KEY @"Image"
#define kTAB_ICON_SELECTED_IMAGE_KEY @"Selected_Image"
#define kTAB_ICON_INDEX_KEY @"Index"

//Logger
#define kLOG_ID_KEY @"log_id"
#define kLOG_ACTION_KEY @"log_action"
#define kLOG_CATEGORY_KEY @"log_category"
#define kLOG_CATEGORY_ID_KEY @"log_category_id"
#define kLOG_DATE_KEY @"log_date"

//Team
#define kTEAM_ID_KEY @"team_id"
#define kTEAM_NAME_KEY @"team_name"
#define kTEAM_IMAGE_KEY @"team_image"
#define kTEAM_TEXT_KEY @"team_text"
#define kTEAM_INSERT_DATE_KEY @"team_insert_date"

//Catalogue
#define kCAT_ID_KEY @"cat_id"
#define kCAT_NAME_KEY @"cat_name"
#define kCAT_IMAGE_KEY @"cat_image"
#define kCAT_INSERT_DATE_KEY @"cat_insert_date"
#define kCAT_MEDIA_KEY @"cat_media"
#define kCAT_MEDIA_ID_KEY @"cat_media_id"
#define kCAT_MEDIA_IMAGE_KEY @"cat_media_image"
#define kCAT_MEDIA_DATE_KEY @"cat_media_insert_date"

//Video
#define kVIDEO_ID_KEY @"video_id"
#define kVIDEO_TITLE_KEY @"video_title"
#define kVIDEO_IMAGE_KEY @"video_image"
#define kVIDEO_EMBED_KEY @"video_embed"
#define kVIDEO_INSERT_DATE_KEY @"video_insert_date"

//News
#define kNEWS_ID_KEY @"news_id"
#define kNEWS_TITLE_KEY @"news_title"
#define kNEWS_IMAGE_KEY @"news_image"
#define kNEWS_TEXT_KEY @"news_text"
#define kNEWS_INSERT_DATE_KEY @"news_insert_date"


