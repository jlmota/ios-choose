//
//  UIFontConstants.h
//  Choose
//
//  Created by João Luís Mota on 10/11/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#define kFONT_BEBAS(s) [UIFont fontWithName:@"Bebas" size:s]
#define kFONT_BEBAS_NEUE(s) [UIFont fontWithName:@"BebasNeue" size:s];