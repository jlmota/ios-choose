//
//  AppConstant.h
//  Choose
//
//  Created by João Luís Mota on 5/19/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//

#define kHEADER_TITLE_IMAGE @"header_logo.png"
#define kHEADER_BACK_IMAGE @"header_bt_back.png"

#define kTAB_ICOB_ICON_WIDTH 64

#define kTAB_BAR_BACKGROUND @"footer_background.png"
#define kTAB_BAR_HEIGHT 49

#define kINTERNET_WARNING_TITLE @""
#define kINTERNET_WARNING_MESSAGE @""

#define kAPP_VERSION @1.0

#define kCHAppDelegate ((CHAppDelegate *)[UIApplication sharedApplication].delegate)

#define kABOUT_TEXT @"\nUma marca de streetwear portuguesa criada em 2008 e lançada no mercado em 2009.\n\nO design simples, a atenção ao pormenor e a preocupação em criar, não só peças de vestuário originais, mas também uma maneira estimulante de olhar para as mesmas, é uma prioridade constante da marca.\n\nOs nossos designers trabalham todos os dias para ir ao encontro das escolhas de cada cliente, inspirando-se no mundo que nos rodeia e nas tendências internacionais.\n\nColecções temáticas e o relacionamento da marca com os desportos de acção ajudam-nos a criar ligações com os nossos clientes e seguidores.\n\nOs desportos de acção são uma área onde queremos ter um papel importante, acreditamos nas capacidades dos jovens e valorizamos o talento em cada um deles, estamos prontos para ajudar esses sedentos de adrenalina a ultrapassar os seus próprios limites.\n\nTu escolhes!\nChoose Clothing\n"
