//
//  GAConstants.h
//  Choose
//
//  Created by João Luís Mota on 05/08/15.
//  Copyright (c) 2015 João Luís Mota. All rights reserved.
//

static NSString * const kGOOGLE_ANALITICS_TRAKER_ID = @"UA-65974179-1";

static NSString * const kGA_SCREEN_NAME_NEWS = @"News";
static NSString * const kGA_SCREEN_NAME_NEWS_DETAILS = @"News details";
static NSString * const kGA_SCREEN_NAME_COLLECTION = @"Collection";
static NSString * const kGA_SCREEN_NAME_COLLECTION_DETAILS = @"Collection details";
static NSString * const kGA_SCREEN_NAME_VIDEOS = @"Videos";
static NSString * const kGA_SCREEN_NAME_TEAM_RIDERS = @"Team Riders";
static NSString * const kGA_SCREEN_NAME_TEAM_RIDERS_DETAILS = @"Team Riders details";
static NSString * const kGA_SCREEN_NAME_ABOUT = @"About";
static NSString * const kGA_SCREEN_NAME_MENU = @"Menu";

static NSString * const kGA_TEAM_CATEGORY = @"TeamRider";
static NSString * const kGA_COLLECTION_CATEGORY = @"Collection";
static NSString * const kGA_NEWS_CATEGORY = @"News";
static NSString * const kGA_ABOUT_CATEGORY= @"About";

static NSString * const kGA_DETAIL_TAPPED_EVENT = @"DetailOpened";
static NSString * const kGA_BUTTON_TAPPED_EVENT = @"ButtonTapped";