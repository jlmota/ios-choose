//
//  SegueConstants.h
//  Choose
//
//  Created by João Luís Mota on 10/11/13.
//  Copyright (c) 2013 Fullsix. All rights reserved.
//

#define kNEWS_HEADER_VIEW_NIB @"NewsHeaderView"
#define kNEWS_CONTENT_VIEW_NIB @"NewsContentView"
#define kTEAM_HEADER_VIEW_NIB @"TeamHeaderView"
#define kTEAM_CONTENT_VIEW_NIB @"TeamContentView"

#define kSMALL_VIEW_CELL_NIB @"SmallViewCell"
#define kLARGE_VIEW_CELL_NIB @"LargeViewCell"
#define kVIDEO_VIEW_CELL_NIB @"VideoViewCell"

