//
//  WSConfig.h
//  Choose
//
//  Created by João Luís Mota on 5/19/13.
//  Copyright (c) 2013 Choose. All rights reserved.
//
//#define kDEFAULT_TIME_STAMP @"2015-07-21T15:30:00"
#define kDEFAULT_TIME_STAMP @"2015-08-05T13:06:00"

#define kLOGGING 0

#define kCH_URL @"http://www.choose.com.pt/app/"
#define kCH_SERVICE @"services.php"

#define kCH_NEWS_METHOD @"news"
#define kCH_VIDEOS_METHOD @"videos"
#define kCH_TEAM_METHOD @"team"
#define kCH_CATALOGUE_METHOD @"catalogue"
#define kCH_CATALOGUES_METHOD @"catalogues"
#define kCH_CATALOGUE_MEDIA_METHOD @"catalogues_media"


#define kADD_TAG @"add"
#define kEDIT_TAG @"edit"
#define kDELETE_TAG @"del"

#define kNULL  @"null"